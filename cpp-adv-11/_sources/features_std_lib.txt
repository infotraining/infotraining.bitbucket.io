*********************************
Nowości w bibliotece standardowej
*********************************

.. toctree::

    hash_containers
    tuples
    smart_pointers
    