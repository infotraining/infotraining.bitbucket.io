Template Method - metoda szablonowa
===================================


Przeznacznie metody szablonowej
-------------------------------

* Definiuje szkielet algorytmu, odkładając implementację niektórych kroków algorytmu do klas podrzędnych.
* Definiując niektóre z kroków algorytmu za pomocą metod abstrakcyjnych, metoda szablonowa ustala ich kolejność, ale umożliwia klasom podrzędnym zmianę implementacji tych kroków zależnie od ich potrzeb.


Kontekst/Problem
----------------

**Kontekst**

* Istnieje algorytm wymagający zmiany implementacji poszczególnych kroków.

**Problem**

* Chcemy jednorazowo zaimplementować stałą część algorytmu i pozostawić klasom pochodnym 
  zaimplementowanie zachowania, które może się zmieniać.
* Chcemy zdefiniować metodę, która w wybranych miejscach wywołuje operacje-punkty zaczepienia, 
  umożliwiając tym samym rozszerzanie klas tylko w tych miejscach.


Struktura i uczestnicy
----------------------

**AbstractClass**

* definiuje abstrakcyjne operacje pierwotne lub operacje-punkty zaczepienia, przedefiniowywane przez 
  podklasy w celu zaimplementowania kroków algorytmu
* implementuje metodę szablonową definiującą szkielet algorytmu
* metoda szablonowa wywołuje operacje pierwotne, jak również operacje zdefiniowane w ``AbstractClass`` 
  lub operacje innych obiektów

.. only:: html

  .. image:: img/TemplateMethod.*
      :align: center
      :height: 300px

.. only:: latex

  .. image:: img/TemplateMethod.*
      :align: center

**ConcreteClass**

* definiuje operacje pierwotne lub zmienia implementacje operacji-punktów zaczepienia w celu wykonania 
  specyficznych dla podklasy kroków algorytmu

Klasa ``ConcreteClass`` polega na ``AbstractClass`` w kwestii implementacji niezmiennych kroków algorytmu.


Konsekwencje
-------------

1. Użycie metod szablonowych jest podstawową techniką stosowaną w celu zagwarantowania możliwości 
   ponownego wykorzystania kodu
2. Metody szablonowe prowadzą do odwróconej struktury sterowania – klasa bazowa wywołuje operacje 
   klasy pochodnej, a nie odwrotnie.
3. Metody szablonowe wywołują:

   * Operacje konkretne z ``ConcreteClass`` lub z klas klienta
   * Operacje konkretne odziedziczone z ``AbstractClass`` – przydatne dla klas pochodnych
   * Operacje pierwotne – operacje abstrakcyjne
   * Metody wytwórcze
   * Operacje punkty-zaczepienia, zapewniające zachowanie domyślne, które może być rozszerzane przez klasy pochodne. Domyślna implementacja operacji punkt zaczepienia często jest pusta (nic nie robi).


Implementacja
-------------

1. Stosowanie mechanizmów sterowania dostępem.

   * W języku C# operacje pierwotne wywoływane przez metodę szablonową można zadeklarować jako składowe chronione, co gwarantuje, że będą wywoływane jedynie przez metodę szablonową.
   * Operacje pierwotne, które muszą być przedefiniowane, deklaruje się jako metody abstrakcyjne. Sama metoda szablonowa nie powinna być przedefiniowywana.

2. Minimalizowanie liczby operacji pierwotnych.
3. Konwencje dotyczące nadawania nazw.


Podsumowanie
------------

* Definiuje szkielet danego algorytmu w określonej metodzie, przekazując realizację niektórych kroków algorytmu do klas podrzędnych
* Pozwala klasom podrzędnym na redefiniowanie pewnych kroków algorytmu, ale jednocześnie uniemożliwia zmianę jego struktury
* Klasy abstrakcyjne wzorca *Template Method* mogą definiować metody rzeczywiste, metody abstrakcyjne oraz metody-punkty zaczepienia
* *Reguła Hollywood* – proces podejmowania decyzji powinien być umieszczony w modułach wysokiego poziomu, które mogą samodzielnie decydować, jak i kiedy wywoływać moduły niskiego poziomu

Pokrewnymi wzorcami do *Template Method* są:

* *Strategy* - podobnie jak *Template Method* dotyczy implementacji algorytmów, lecz realizuje ją w odmienny sposób. Dokonuje hermetyzacji algorytmów odpowiednio wykorzystując kompozycję i dziedziczenie
* *Factory Method* - jest często implementowany razem z *Template Method*. Metoda wytwórcza jes często wywoływana jako krok algorytmu w metodzie szablonowej.






