.. Efektywne wykorzystanie biblioteki standardowej C++
   first entry: Leszek Tarkowski, 23.10.2013

Efektywne wykorzystanie biblioteki standardowej C++
===================================================

.. toctree::
   :maxdepth: 3

   wprowadzenie
   kontenery_standardowe
   adaptory_kontenerow
   iteratory
   alokatory
   funkcje
   algorytmy_standardowe
   parallel-stl
   