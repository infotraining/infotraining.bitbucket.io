﻿S.O.L.I.D. OOP
==============

**SOLID** to akronim powstały od pięciu zasad dobrego programowania zorientowanego obiektowo.

Zasada pojedynczej odpowiedzialności
------------------------------------

**Zasada pojedynczej odpowiedzialności** (*Single-Responsibility Principle*) – każdy obiekt w kodzie powinien mieć tylko jedną odpowiedzialność, a wszystkie usługi tego obiektu powinny koncentrować się na jej realizacji.

Jeśli dla każdego obiektu w systemie istnieje tylko jeden powód do jego modyfikacji, oznacza to, że zasada pojedynczej odpowiedzialności została zaimplementowana prawidłowo.

Każdy obszar odpowiedzialności jest faktyczną osią zmian. Kiedy wymagania ulegają zmianie, zakres niezbędnych modyfikacji jest określany na podstawie obszarów odpowiedzialności przypisanych do poszczególnych klas. Jeśli pojedyncza klasa odpowiada za więcej niż jeden obszar jednocześnie, może istnieć więcej niż jeden powód jej modyfikowania. W takim przypadku, te rozłączne obszary są ze sobą powiązane. Zmiany jednego obszaru mogą osłabiać lub powstrzymywać funkcjonowanie w innym obszarze. Każdy taki związek ogranicza elastyczność projektów i prowadzi do nieoczekiwanych rezultatów zmian.

Przykład naruszenia zasady pojedynczej odpowiedzialności.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Klasa Employee łączy dwa obszary odpowiedzialności – reguły biznesowe oraz mechanizmy utrwalania danych.

.. image:: img/solid/srp.*
    :align: center

Reguły biznesowe z natury rzeczy bardzo często ulegają modyfikacjom. Jeżeli istnieje konieczność zmian mechanizmów utrwalania, przyczyny tych modyfikacji są zupełnie inne niż w przypadku reguł biznesowych. 

Zasada otwarte-zamknięte
------------------------

**Zasada otwarte-zamknięte** (*Open/Close Principle*) – klasy powinny być otwarte na rozbudowę i zamknięte na modyfikacje.

Jeśli dysponujemy klasą o określonym zachowaniu, które działa prawidłowo, należy uniemożliwić wprowadzenie modyfikacji w jej działającym kodzie. W przypadku konieczności rozbudowy kodu, należy stworzyć klasy pochodne, w których można przesłonić wybrane metody i dostosować ich działanie do bieżących potrzeb. Choć nie można modyfikować kodu klasy (zasada zamknięte), to jednak jest ona otwarta na rozbudowę (zasada otwarte).

Dziedziczenie jest prostym przykładem zasady OCP, jednak zasada ta dotyczy elastyczności i wykracza poza samo dziedziczenie. Zwykle dziedziczenie jest najłatwiejsze do zaimplementowania, ale nie jest to jedyna możliwość. Jeśli mamy w klasie kilka metod prywatnych, to są one zamknięte na modyfikacje. Możemy jednak dodać do klasy kilka metod publicznych, które będą wywoływać te metody prywatne na różne sposoby. Rozszerzamy zachowanie metod prywatnych bez jednoczesnego modyfikowania ich kodu.

Przykład naruszenia zasady OCP:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: img/solid/ocp-before.*
    :align: center


W tym przypadku konkretna klasa Client wykorzystuje konkretną klasę Server.

Gdybyśmy chcieli aby obiekt klasy Client wykorzystywał obiekt innej klasy serwera, musielibyśmy w klasie Client zmienić nazwę wykorzystywanej klasy serwera.

Można przekształcić ten projekt, tak aby stał się on zgodny z zasadą OCP, stosując wzorzec Strategia.

.. image:: img/solid/ocp-after.*
    :align: center

Zasada OCP jest kombinacją hermetyzacji i wyodrębniania. Określamy zachowania, które pozostają takie same, wyodrębniamy je i definiujemy w klasie bazowej, uniemożliwiając modyfikowanie tego kodu. Jeśli potrzebujemy nowego lub zmienionego zachowania, tworzymy klasę pochodną, która je obsłuży. Hermetyzujemy to, co ulega zmianom (zachowanie w klasach pochodnych), oddzielając od tych fragmentów aplikacji, które pozostają niezmienione (wspólne zachowanie zdefiniowane w klasie bazowej).

Zasada podstawiania Liskov
--------------------------

**Zasada podstawiania Liskov** (*Liskov Substitution Principle*) – musi istnieć możliwość podstawiania typów pochodnych w miejsce ich typów bazowych.

LCP dotyczy prawidłowo zaprojektowanego dziedziczenia. Tworząc klasę pochodną, musimy być w stanie użyć jej zamiast klasy bazowej. Jeśli nie ma takiej możliwości, to dziedziczenie zostało nieprawidłowo użyte.

Przykład naruszenia zasady podstawiania:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: img/solid/lsp.*
    :align: center

Oprócz dziedziczenia istnieje kilka technik używania zachowań zdefiniowanych w innych klasach:

* Delegowanie – wykonanie zadania można delegować do innej klasy, jeśli nie chcemy zmieniać sposobu jego realizacji, a jego implementacja nie należy do odpowiedzialności danego obiektu.
* Kompozycja – zebranie niezbędnych zachowań zdefiniowanych w jednej lub kilku klasach, a także w całych rodzinach klas. Wszystkie obiekty należące do kompozycji całkowicie należą do obiektu, który ich używa i nie mogą istnieć poza nim.
* Agregacja – używana, kiedy chcemy skorzystać z zachowań zdefiniowanych w innych klasach, bez ograniczania czasu życia tych zachowań. 

Zasada segregacji interfejsów
-----------------------------

**Zasada segregacji interfejsów** (Interface Segregation Principle) – klient nie powinien być zmuszany do zależności od metod, których nie używa.

Jeśli klient zależy od klasy zawierającej metodę, której ten klient nie używa, ale której używają pozostałe klasy klienckie, to zmiany tych klas będą miały wpływ na naszą klasę. Aby uniknąć tego rodzaju związków, należy podzielić interfejsy.

Przykład naruszenia zasady segregacji interfejsów:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: img/solid/isp-before.*
    :align: center

Najlepszym sposobem zapewnienia zgodności z zasadą ISP jest zastosowanie techniki dziedziczenia wielokrotnego. Mimo że obiekty klienckie dwóch klas bazowych mogą korzystać z tego samego interfejsu, żadna z tych klas nie jest zależna od tego interfejsu. Oznacza to, że obiekty kliencie korzystają z tego samego obiektu za pośrednictwem różnych interfejsów.

Poprawiony diagram klas zgodny z zasadą SRP.

.. image:: img/solid/isp-after.*
    :align: center

Obiekty klienckie powinny zależeć wyłącznie od wywoływanych przez siebie metod. Można ten cel osiągnąć rozbijając interfejs "grubej" klasy na wiele mniejszych interfejsów właściwych dla poszczególnych klientów. Każdy taki interfejs deklaruje tylko te funkcje, które rzeczywiście są wywoływane przez danego klienta lub grupę klientów. W takim przypadku "gruba" klasa może dziedziczyć i implementować wszystkie interfejsy właściwe dla klientów. Opisany model eliminuje zależność obiektów klienckich od metod, których nie wywołują, i umożliwia zapewnienie wzajemnej niezależności samych klientów.

Zasada odwracania zależności
----------------------------

**Zasada odwracania zależności** (*Dependency Inversion Principle*)

* Moduły wysokopoziomowe nie powinny zależeć od modułów niskopoziomowych. Obie grupy modułów powinny zależeć od abstrakcji.
* Abstrakcje nie powinny zależeć od szczegółowych rozwiązań. To szczegółowe rozwiązania powinny zależeć od abstrakcji.

Tradycyjnie programowanie proceduralne prowadzi do powstawania struktur złożoności, w których ogólna strategia jest uzależniona od szczegółowych rozwiązań w zakresie implementacji. Istnienie takich zależności jest o tyle niekorzystne, że czyni strategię wrażliwą na zmiany szczegółów. Programowanie obiektowe odwraca tę strukturę zależności w taki sposób, aby zarówno szczegóły, jak i strategie zależały od abstrakcji. Co więcej, w modelu obiektowym interfejsy usług są często przypisane do swoich klientów. Odwracanie zależności jest swoistym certyfikatem dobrego projektu obiektowego.

Zasada odwracania zależności jest kluczowym mechanizmem niskiego poziomu. Właściwe stosowanie tej zasady jest ważne, jeśli chcemy tworzyć frameworki wielokrotnego użytku. DIP ma także ogromny wpływ na odporność kodu źródłowego na przyszłe zmiany. Ponieważ zgodnie z tą zasadą abstrakcje i szczegółowe mechanizmy są od siebie izolowane, tak tworzony kod jest dużo prostszy w konserwacji.

Zasadę odwracania zależności możemy stosować za każdym razem, gdy jedna klasa wysyła komunikat do innej klasy.

Przanalizujmy przykładowy schemat naruszenia zasady DIP:

.. image:: img/solid/dip-before.*
    :align: center

Klasa ``Button`` bezpośrednio zależy od klasy ``Lamp``. Wysokopoziomowa strategia nie została właściwie oddzielona od niskopoziomowej implementacji. Abstrakcje nie zostały oddzielone od szczegółowych rozwiązań. Model ten można udoskonalić, odwracając zależność łączącą obiekty klas ``Button`` i ``Lamp``. 
 
Poniżej przedstawiony jest model, w którym klasa ``Button`` zawiera odwołanie tylko do interfejsu nazwanego ``ButtonServer`` – za jego pośrednictwem można włączać lub wyłączać różne urządzenia. 

.. image:: img/solid/dip-after.*
    :align: center

Interfejs ``ButtonServer`` jest implementowany przez klasę ``Lamp``. Oznacza to, że od tej pory to klasa ``Lamp`` zależy od klasy ``Button`` (a nie odwrotnie).