Composite - kompozyt
====================


Przeznaczenie
-------------


* Składa obiekty w struktury drzewiaste reprezentujące hierarchie typu część-całość.
* Umożliwia klientom jednakowe traktowanie pojedynczych obiektów i złożeń (agregatów) obiektów.
* Kluczem do wzorca ``Composite`` jest klasa abstrakcyjna, która reprezentuje zarówno elementy pierwotne jak i ich pojemniki.


Kontekst/Problem
----------------

**Kontekst**

* Chcemy przedstawić hierarchie obiektów część-całość.
* Hierarchia obiektów ma wspólną klasę bazową (klasę abstrakcyjną lub interfejs).

**Problem**

* Chcemy, aby klienci mogli ignorować różnicę między złożeniami obiektów a pojedynczymi obiektami. Klienci będą wtedy jednakowo traktować wszystkie obiekty występujące w strukturze.

.. only:: html

  .. image:: img/CompositeShape.*
    :align: center
    :height: 170pt

.. only:: latex

  .. image:: img/CompositeShape.*
    :align: center
    :width: 60%


Struktura i uczestnicy
----------------------

**Component**

* Implementuje, tam gdzie to możliwe, domyślne zachowanie w wypadku interfejsu wspólnego 
  dla wszystkich klas
* Definiuje interfejs umożliwiający dostęp i zarządzanie komponentami-dziećmi
* Definiuje interfejs umożliwiający dostęp do rodzica komponentu w strukturze rekurencyjnej i 
  implementuje go, o ile jest to potrzebne

.. only:: html

  .. image:: img/Composite.*
    :align: center
    :height: 200pt

.. only:: latex

  .. image:: img/Composite.*
    :align: center


**Leaf**

* Reprezentuje obiekty będące liśćmi w składanej strukturze; liść nie ma dzieci
* Definiuje zachowanie obiektów pierwotnych w strukturze

**Composite**

* Definiuje zachowanie komponentów mających dzieci
* Przechowuje komponenty będące dziećmi
* Implementuje operacje z interfejsu komponentu związane z dziećmi

**Client** – manipuluje obiektami występującymi w strukturze, wykorzystując do tego interfejs komponentu

Współpraca
----------

Klienci używają interfejsu z klasy ``Component`` w celu komunikowania się z obiektami występującymi w składanej strukturze. Jeśli odbiorca jest liściem, to żądania są realizowane bezpośrednio. Jeśli odbiorca jest kompozytem (``Composite``), to zwykle przekazuje (deleguje) swoje żądania komponentom-dzieciom, wykonując ewentualnie przed i/ lub po przekazaniu dodatkowe operacje.


Konsekwencje
------------

1. Wzorzec *Composite* definiuje hierarchie klas grupujących obiekty pierwotne i złożone.
2. Uproszczenie budowy klienta – klienci mogą jednakowo traktować struktury złożone i pojedyncze obiekty.
3. Ułatwienie dodawania nowych rodzajów komponentów.
4. Może sprawić, że projekt będzie zbyt ogólny. Umieszczenie operacji dodawania nowych komponentów w klasie bazowej ``Component`` komplikuje wprowadzanie ograniczeń dotyczących złożeń komponentów.

Implementacja
-------------

1. Jawne odwołania do rodziców. 

   * Przechowywanie odwołań z komponentów-dzieci do ich rodziców może ułatwiać poruszanie się po strukturze kompozytu i zarządzanie nią. Typowym miejscem do definiowania odwołania do rodzica jest klasa ``Component``. Klasy ``Leaf`` i ``Composite`` mogą dziedziczyć to odwołanie.

2. Współdzielenie komponentów. Bardzo często opłacalne jest współdzielenie komponentów, na przykład w celu ograniczenia wymagań pamięciowych

3. Jaka struktura danych jest najlepsza do przechowywania komponentów? 

   * Kompozyty mogą używać wielu różnych struktur danych do przechowywania swoich dzieci: list, drzew, tablic i tablic z haszowaniem.

4. Ustalenie, które klasy w hierarchii klas ``Composite`` deklarują operacje ``Add()`` i ``Remove()`` dotyczące zarządzania dziećmi - wybór między bezpieczeństwem a przezroczystością interfejsów. 

   * Zdefiniowanie interfejsu zarządzania dziećmi w korzeniu omawianej hierarchii klas. Zapewnia przezroczystość, ponieważ umożliwia jednakowe traktowanie wszystkich komponentów. Zmniejsza bezpieczeństwo implementacji, ponieważ klienci mogą próbować dodawać i usuwać obiekty z liści. 
   * Zdefiniowanie zarządzania dziećmi w klasie ``Composite``. Zapewnia bezpieczeństwo implementacji, ponieważ każda próba dodawania lub usuwania obiektów z liści będzie w językach ze statyczną kontrolą typów, takich jak C#, wychwycona już w czasie kompilacji.


Wzorce pokrewne
---------------

1. Związku komponent-rodzic używa się często przy stosowaniu wzorca *Chain Of Resposibility*.
2. Wzorzec *Composite* jest często używany z *Dekoratorem*. Gdy *Dekorator* i *Kompozyt* są stosowane razem, mają na ogół wspólnego rodzica. Klasa dekoratora musi zatem uwzględnić interfejs klasy ``Component`` z takimi operacjami, jak ``Add()``, ``Remove()`` i ``GetChild()``.
3. Wzorzec *Flyweight* umożliwia współdzielenie komponentów, ale nie mogą one już mieć referencji do swoich rodziców.
4. Wzorzec *Iterator* może być używany do przechodzenia kompozytów.
5. Wzorzec *Visitor* grupuje w jednym miejscu operacje i zachowanie, które w przeciwnym razie byłyby rozproszone w klasach ``Composite`` i ``Leaf``.


