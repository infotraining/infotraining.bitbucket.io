**********
Wzorce GOF 
**********

.. toctree::
    :maxdepth: 1

    gof-patterns/factory_method
    gof-patterns/abstract-factory
    gof-patterns/prototype
    gof-patterns/builder
    gof-patterns/adapter
    gof-patterns/decorator
    gof-patterns/composite
    gof-patterns/proxy
    gof-patterns/facade
    gof-patterns/bridge
    gof-patterns/flyweight
    gof-patterns/template_method
    gof-patterns/strategy
    gof-patterns/state
    gof-patterns/chain
    gof-patterns/command
    gof-patterns/memento
    gof-patterns/observer
    gof-patterns/mediator
    gof-patterns/visitor
    gof-patterns/iterator
        