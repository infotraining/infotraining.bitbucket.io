Facade
======

**Przeznaczenie**

* Zapewnia jeden, zunifikowany interfejs dla całego zestawu interfejsów określonego podsystemu
* Tworzy nowy interfejs wysokiego poziomu, który powoduje, że korzystanie z całego podsystemu staje się zdecydowanie łatwiejsze

**Kontekst**

* Klient zmuszony jest do bezpośredniego stosowania złożonego podsystemu (biblioteki)

**Problem**

* Chcemy odseparować klienta od bezpośredniego stosowania złożonych podsystemów

.. image:: images/uml-schemes/facade-comp.*
    :width: 500px
    :align: center

Uczestnicy
----------

**Fasada**

* wie jakie klasy podsystemu są odpowiedzialne za spełnienie żądania
* przekazuje żądania klienta do odpowiednich obiektów podsystemu

**Klasy podsystemu**

* implementują funkcje podsystemu
* wykonują pracę przydzieloną przez obiekt klasy *Facade*
* nic nie wiedzą o fasadzie, nie przechowują żadnych odwołań do niej

Współpraca
----------

* Klienci komunikują się z podsystemem, wysyłając żądania do fasady (*Facade*), która przekazuje je do odpowiednich obiektów podsystemu
* Fasada może być zmuszona realizować swoje zadania, polegające na tłumaczeniu jej interfejsu na interfejsy podsystemu
* Klienci wykorzystujący fasadę nie muszą mieć bezpośredniego dostępu do obiektów z jej podsystemu

Konsekwencje
------------

1. Oddziela klientów od komponentów podsystemu, dzięki czemu zmniejsza się liczba obiektów, z którymi klienci mają do czynienia. Podsystem staje się łatwiejszy do użycia.
2. Sprzyja słabemu powiązaniu podsystemu z jego klientami. Umożliwia zmianę komponentów w sposób niewidoczny dla jego klientów.
3. Ułatwia ułożenie warstwami systemu i zależności między obiektami.
4. Nie uniemożliwia aplikacjom bezpośredniego dostępu do podsystemu, jeśli potrzebują.

Implementacja
-------------

1. Zredukowanie powiązań klient-podsystem. Można ograniczyć powiązanie czyniąc *Facade* klasą abstrakcyjną z podklasami konkretnymi dla różnych implementacji podsystemu. Klienci mogą komunikować się z podsystemem poprzez interfejs klasy abstrakcyjnej *Facade*. Konfigurowanie obiektu *Facade* za pomocą różnych obiektów podsystemu.
2. Publiczne a prywatne klasy podsystemu. Publiczny interfejs podsystemu składa się z klas, do których dostęp mają wszyscy klienci. Klasa fasada jest częścią interfejsu publicznego podsystemu.

Wzorce pokrewne
---------------

1. **Abstract Factory** – wzorzec fasada można użyć ze wzorcem *AbstractFactory*. zapewni się w ten sposób interfejs do tworzenia obiektów podsystemów w sposób niezależny od podsystemów.
2. **Singleton** – zwykle potrzeby jest jeden obiekt będący fasadą, dlatego fasady często są implementowane jako singletony.

Podsumowanie
------------

1. Udostępnia interfejs pozwalający ukryć przed klientem złożoność podsystemu.
2. Sprzyja słabemu powiązaniu klientów z podsystemem.
