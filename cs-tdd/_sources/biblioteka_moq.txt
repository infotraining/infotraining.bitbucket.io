Biblioteka Moq
==============

Dodawanie Moq do projektu Visual Studio
---------------------------------------

Aby dodać bibliotekę Moq do projektu zawierającego testy, musimy zainstalować pakiet NuGet za pomocą Console Package Manager:
Moq

.. code:: sh

    Install-Package Moq

Bibliotekę można też pobrać z ``http://code.google.com/p/moq`` i dodać referencję do zestawu Moq.dll.

Tworzenie obiektów pozorujących za pomocą Moq
---------------------------------------------

Narzędzia imitacji umożliwiają tworzenie takich obiektów pozorujących, które mają dokładnie takie funkcje, jakie są potrzebne w naszym teście. Dzięki temu nie ma niebezpieczeństwa wytworzenia obiektu pozorującego, który jest zbyt skomplikowany. W rzeczywistym projekcie, możemy szybko osiągnąć etap, w którym sam obiekt pozorujący będzie wymagał osobnego testowania, ponieważ będzie zawierać dużo kodu. Możemy tworzyć wiele małych, ręcznych imitacji, ale aby było to efektywne, potrzebujemy przenoszenia powtarzalnego kodu do klas bazowych, co ponownie zwiększa złożoność. Testy jednostkowe działają najlepiej, gdy są małe i precyzyjnie ukierunkowane, aby zapewnić maksymalną prostotę. 

Istnieją dwa etapy tworzenia imitacji za pomocą Moq. W pierwszym tworzymy nowy obiekt ``Mock<T>``, gdzie T jest imitowanym typem.

.. code:: csharp

    Mock<IProductRepository> mock = new Mock<IProductRepository>();

W drugim etapie konfigurujemy zachowanie, jakie ma pozorować nasza implementacja. Moq automatycznie implementuje wszystkie metody i właściwości podanego typu, ale wykorzystuje wartości domyślne typów. Na przykład, metoda ``IProductRepository.GetProducts()`` zwraca pustą kolekcję ``IEnumerable<Product>``. Aby zmienić sposób implementowania składnika przez Moq, musimy użyć metody ``Setup()``.

.. code:: csharp

    Product[] products = new Product[] {
        new Product { Name = "Kajak", Price = 275M },
        new Product { Name = "Kamizelka ratunkowa", Price = 48.95M },
        new Product { Name = "Piłka nożna", Price = 19.50M },
        new Product { Name = "Stadion", Price = 79500M },
    };

    mock.Setup(m => m.GetProducts()).Returns(products);

Podczas konfigurowania żądanego zachowania Moq korzysta z wyrażeń lambda. Gdy wywołujemy metodę ``Setup()``, jako argument podajemy funkcję lambda, której argument jest typu, którego implementacji zażądaliśmy. Gdy chcemy zdefiniować zachowanie dla metody ``GetProducts()``, korzystamy z następującego kodu:

.. code:: csharp

    mock.Setup(m => m.GetProducts()).(<...inne metody...>);

Metoda ``GetProducts()`` jest prosta do obsłużenia, ponieważ nie ma parametrów.

Aby skonfigurować metody posiadające parametry, musimy użyć filtra parametrów. Możemy skonfigurować bibliotekę Moq tak, aby odpowiadała inaczej w zależności od wartości parametrów przekazanych do metody. Metoda ``GetProducts()`` nie posiada parametru, więc do demonstracji tego mechanizmu użyjemy następującego prostego interfejsu:

.. code:: csharp

    public interface IMyInterface 
    {
        string ProcessMessage(string message);
    }

Poniżej znajduje się kod tworzący imitację tego interfejsu z różnymi zachowaniami dla różnych wartości parametru.

.. code:: csharp

    Mock<IMyInterface> mock  = new Mock<IMyInterface>();
    mock.Setup(m => m.ProcessMessage("Cześć")).Returns("Witaj");
    mock.Setup(m => m.ProcessMessage("Do widzenia")).Returns("Do zobaczenia");

Gdy wartością parametru metody ``ProcessMessage()`` jest ``Cześć``, zwracany jest napis ``Witaj``, natomiast jeżeli wartością parametru jest ``Do widzenia``, zwracany jest napis ``Do zobaczenia``. Dla wszystkich innych parametrów Moq zwróci domyślną wartość typu wynikowego, czyli w w tym przypadku ``null``, ponieważ jest to typ ``string``. 

Konfigurowanie odpowiedzi dla wszystkich możliwych wartości parametrów szybko staje się nużące i trudne w przypadku bardziej złożonych typów, ponieważ konieczne jest tworzenie reprezentujących je obiektów i używanie ich przy porównaniach. Na szczęście Moq posiada klasę ``It``, za pomocą której możemy reprezentować szerszą kategorię wartości parametrów.

.. code:: csharp

    mock.Setup(m => m.ProcessMessage(It.IsAny<string>())).Returns("Otrzymano komunikat");

W klasie ``It`` zdefiniowanych jest kilka metod posiadających generyczne typy parametrów. W tym przykładzie wywołaliśmy metodę ``IsAny`` z użyciem ``string`` jako typu generycznego. Informujemy w ten sposób Moq, że gdy metoda ``ProcessMessage`` zostanie wywołana z dowolną wartością znakową, powinna zwrócić odpowiedź ``Otrzymano komunikat``. 

W tabeli wymienione są statyczne metody klasy ``It``

+--------------------------+-------------------------------------------------------------------------------+
| Metoda                   | Opis                                                                          |
+==========================+===============================================================================+
| Is<T>()                  | Dopasowanie bazuje na podanym predykacie                                      |
+--------------------------+-------------------------------------------------------------------------------+
| IsAny<T>()               | Dopasowuje wartości, gdy parametr jest dowolnym obiektem typu T               |
+--------------------------+-------------------------------------------------------------------------------+
| IsInRange<T>()           | Dopasowuje wartości, jeśli parametr jest pomiędzy zdefiniowanymi wartościami  |
+--------------------------+-------------------------------------------------------------------------------+
| IsRegex                  | Dopasowuje wartości, jeśli pasują do podanego wyrażenia regularnego           |
+--------------------------+-------------------------------------------------------------------------------+

Metoda ``Is<T>`` jest najbardziej elastyczna, ponieważ pozwala dostarczyć predykat, który dopasowuje wartość parametru, jeśli zwróci on wartość ``true``.

.. code:: csharp

    mock.Setup(m => m.ProcessMessage(It.Is<string>(s => s == "Cześć" || s == "Do widzenia")))
        .Returns("Otrzymano komunikat");

Instrukcja ta informuje Moq, że jeśli wartością parametru jest ``Cześć`` lub ``Do widzenia``, musi zostać zwrócony napis ``Otrzymano komunikat``.

Konfigurując zachowanie, często robimy to w celu zdefiniowania wyniku zwracanego przez metodę. W powyższym przykładzie do wywołania ``Setup()`` dołączona jest metoda ``Returns()``, co pozwala określić zwracaną wartość. Możemy również użyć w metodzie ``Returns()`` parametru przekazywanego do imitowanej metody, dzięki czemu wynik może bazować na danych wejściowych.

.. code:: csharp

    mock.Setup(m => m.ProcessMessage(It.IsAny<string>()))
        .Returns<string>(s => string.Format("Otrzymano komunikat: {0}", s));

Wystarczy, że wywołamy metodę ``Returns()`` z parametrem typu generycznego, który odpowiada parametrowi metody. Moq przekazuje parametr metody do naszego wyrażenia lambda, dzięki czemu możemy wygenerować dynamiczny wynik - w naszym przykładzie tworzymy sformatowany ciąg znaków.

Inne przykłady dopasowania argumentów:

.. code:: csharp

    // dopasowanie z wykorzystaniem delegata Func<int, bool> (leniwa ewaluacja)
    mock.Setup(foo => foo.Add(It.Is<int>(i => i % 2 == 0))).Returns(true); 


    // dopasowanie w zakresie
    mock.Setup(foo => foo.Add(It.IsInRange<int>(0, 10, Range.Inclusive))).Returns(true); 


    // dopasowanie do wyrażenia regularnego
    mock.Setup(x => x.DoSomething(It.IsRegex("[a-d]+", RegexOptions.IgnoreCase))).Returns("foo");

Mockowanie właściwości
----------------------

Konfiguracja obiektu mock, w celu imitacji właściwości wygląda następująco:

.. code:: csharp

    mock.Setup(foo => foo.Name).Returns("bar");

Możliwe jest automatyczne mockowanie właściwości hierarchicznych (rekursywnych)

.. code:: csharp

    mock.Setup(foo => foo.Bar.Baz.Name).Returns("baz");

Możemy również skonfigurować oczekiwane wywołanie settera z określonym argumentem:

.. code:: csharp

    mock.SetupSet(foo => foo.Name = "foo");

    // lub zweryfikować settera bezpośrednio
    mock.VerifySet(foo => foo.Name = "foo");

Zdarzenia
---------

Wyzwolenie zdarzenia na obiekcie mock:

.. code:: csharp

    mock.Raise(m => m.FooEvent += null, new FooEventArgs(fooValue));

Możemy skonfigurować wyzwolenie zdarzenia w odpowiedzi na wywołanie na obiekcie mock wybranej metody:

.. code:: csharp

    mock.Setup(foo => foo.Submit()).Raises(f => f.Sent += null, EventArgs.Empty);

Tak wyzwolone zdarzenie wywołuje reakcję na testowanym obiekcie (SUT), której efekt może być potem sprawdzony asercją.

Funkcje zwrotne (Callbacks)
---------------------------

Obiekty mock, mogą wywoływać funkcje zwrotne. Są one implementowane za pomocą wyrażeń lambda.

Przykład konfiguracji wywołań funkcji zwrotnych:

.. code:: csharp

    List<string> calls = new List<int>();

    mock.Setup(foo => foo.Execute(It.IsAny<string>()))
        .Returns(true)
        .Callback((string s) => calls.Add(s));

Funkcje zwrotne mogą być wywoływane przed lub po inwokacji konfigurowanej funkcji:

.. code:: csharp

    mock.Setup(foo => foo.Execute("ping"))
        .Callback(() => Console.WriteLine("Before returns"))
        .Returns(true)
        .Callback(() => Console.WriteLine("After returns"));

Testy jednostkowe z użyciem Moq
-------------------------------

Po skonfigurowaniu wymaganych zachowań można uzyskać imitowaną implementację poprzez właściwość ``Mock.Object``. Poniżej zaprezentowane jest zastosowanie Moq w teście jednostkowym ``Correct_Total_Reduction_Amount``

.. code:: csharp

    [Test]
    public void Correct_Total_Reduction_Amount() {
        
        // Arrange
        Product[] products = new Product[] {
            new Product { Name = "Kajak", Price = 275M },
            new Product { Name = "Kamizelka ratunkowa", Price = 48.95M },
            new Product { Name = "Piłka nożna", Price = 19.50M },
            new Product { Name = "Stadion", Price = 79500M },
        };

        Mock<IProductRepository> mock = new Mock<IProductRepository>();
        mock.Setup(m => m.GetProducts()).Returns(products);
        decimal reductionAmount = 10;
        decimal initialTotal = products.Sum(p => p.Price);
        MyPriceReducer target = new MyPriceReducer(mock.Object);

        // Act
        target.ReducePrices(reductionAmount);

        // Assert
        Assert.AreEqual(products.Sum(p => p.Price),
            (initialTotal - (products.Count() * reductionAmount)));
    }

Zaimplementowaliśmy tylko tyle funkcji definiowanych w interfejsie ``IProductRepository``, ile potrzeba, aby wykonać nasz test. W tym przypadku oznacza to implementację metody ``GetProducts()``, aby zwróciła nasze dane testowe.

Możemy uprościć ten kod wykorzystując kilka funkcji obsługi testów z biblioteki NUnit. Wiemy, że wszystkie nasze metody testowe będą używać tych samych testowych obiektów ``Product``, więc możemy utworzyć je w klasie testowej w następujacy sposób:

.. code:: csharp
    
    [TestFixture]
    public class MyPriceReducerTest 
    {
        private IEnumerable<Product> products;

        [SetUp]
        public void PreTestInitialize() 
        {
            products = new Product[] {
                new Product { Name = "Kajak", Price = 275M },
                new Product { Name = "Kamizelka ratunkowa", Price = 48.95M },
                new Product { Name = "Piłka nożna", Price = 19.50M },
                new Product { Name = "Stadion", Price = 79500M },
            };
        }
    
    // testy ...

Na początku każdego testu chcemy otrzymać czyste, niezmienione dane testowe, więc utworzyliśmy pole ``products`` i skorzystaliśmy z funkcji poprzedzonej atrybutem ``SetUp`` do zainicjowania danych. Oznacza to, że pole ``product`` będzie ponownie inicjowane świeżymi danymi testowymi. 

W poniższej tabeli zamieszczone są inne atrybuty testów jednostkowych obsługiwane przez Visual Studio:


+--------------------------+----------------------------------------------------------------------------------------------+
| Atrybut                  | Opis                                                                                         |
+==========================+==============================================================================================+
| TestFixtureSetUp         | Metoda wywoływana przed wykonaniem testów z danej klasy. Musi to być metoda statyczna        |
+--------------------------+----------------------------------------------------------------------------------------------+
| TestFixtureTearDown      | Metoda wywoływana po wykonaniu wszystkich testów z danej klasy. Musi to być metoda statyczna |
+--------------------------+----------------------------------------------------------------------------------------------+
| SetUp                    | Metoda wywoływana przed wykonaniem każdego testu                                             |
+--------------------------+----------------------------------------------------------------------------------------------+
| TearDown                 | Metoda wywoływana po wykonaniu każdego testu                                                 |
+--------------------------+----------------------------------------------------------------------------------------------+

Nazwa metody, do której stosowane są te atrybuty, nie ma znaczenia, ponieważ narzędzie automatyzacji testów (test runner) wyszukuje wyłącznie na podstawie atrybutów. Gdy użyjemy atrybutu ``SetUp``, możemy utworzyć i skonfigurować naszą imitację specyficzną dla testu, wykorzystując tylko dwa wiersze kodu:

.. code:: csharp

    Mock<IProductRepository> mock = new Mock<IProductRepository>();
    
    mock.Setup(m => m.GetProducts()).Returns(products);

Weryfikowanie przy użyciu Moq
-----------------------------

Jednym z kryteriów testu było wywołanie metody ``UpdateProduct()`` dla każdego przetwarzanego obiektu ``Product``. W klasie ``FakeRepository`` zrealizowaliśmy to poprzez zdefiniowanie właściwości i zwiększanie jej w metodzie ``UpdateProduct()``. Ten sam efekt możemy uzyskać za pomocą Moq.

.. code:: csharp

    // działanie
    target.ReducePrices(reductionAmount);

    // asercje
    foreach (Product p in products) {
        mock.Verify(m => m.Update.Product(p), Times.Once());
    }

Przy użyciu filtra parametrów możemy sprawdzić, czy metoda ``UpdateProduct()`` została wywołana dokładnie jeden raz dla każdego testowego obiektu ``Product``. Oczywiście możemy to zrealizować z wykorzystaniem ręcznej imitacji, ale podoba nam się prostota zapewniana przez narzędzie imitujące.
