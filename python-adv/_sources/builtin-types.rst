*************************************************
Optymalne wykorzystanie wbudowanych typów Pythona
*************************************************

Wydajność operacji na typach wbudowanych
========================================

Szybkość działania
------------------

Python ma opinię języka, w którym szybko się tworzy, natomiast wykonywany kod jest powolny.
Obecnie komputery są na tyle szybkie, że w większości przypadków można zapomnieć o szybkości działania programu.
Czas programisty jest cenniejszy od czasu komputera.

Jednak przy przetwarzaniu dużej ilości danych może okazać się, że program wykonuje się za wolno.
Pisząc program, którego zadaniem będzie przetwarzanie dużej ilości danych, warto mieć na uwadze, jak szybko wykonują się operacje na kolekcjach takich jak lista, słownik czy zbiór.

Notacja *O* pozwala na zwięzłe opisanie, jak szybko wykonuje się dana operacja.
Zapis *O(1)* oznacza, że dana operacja będzie wykonywała się zawsze tak samo długo, niezależnie od tego, jak duża jest kolekcja.
Zapis *O(n)* oznacza, że czas wykonania danej operacji zależy liniowo od wielkości kolekcji - na przykład, na pustej liście może ona zająć średnio 50 mikrosekund, na jednoelementowej 60 mikrosekund, na dwuelementowej 70 mikrosekund itd.

Wbudowane algorytmy - lista
---------------------------

+---------------+-------------------+---------------------------+
|   Operation   |   Average Case    |   Amortized Worst Case    |
+===============+===================+===========================+
|   Copy        |   O(n)            |   O(n)                    |
+---------------+-------------------+---------------------------+
|   Append[1]   |   O(1)            |   O(1)                    |
+---------------+-------------------+---------------------------+
|   Insert      |   O(n)            |   O(n)                    |
+---------------+-------------------+---------------------------+
|   Get Item    |   O(1)            |   O(1)                    |
+---------------+-------------------+---------------------------+
|   Set Item    |   O(1)            |   O(1)                    |
+---------------+-------------------+---------------------------+
|  Delete Item  |   O(n)            |   O(n)                    |
+---------------+-------------------+---------------------------+
|  Iteration    |   O(n)            |   O(n)                    |
+---------------+-------------------+---------------------------+
|   Get Slice   |   O(k)            |   O(k)                    |
+---------------+-------------------+---------------------------+
|   Del Slice   |   O(n)            |   O(n)                    |
+---------------+-------------------+---------------------------+
|   Set Slice   |   O(k+n)          |   O(k+n)                  |
+---------------+-------------------+---------------------------+
|   Extend[1]   |   O(k)            |   O(k)                    |
+---------------+-------------------+---------------------------+
|   Sort        |   O(n log n)      |   O(n log n)              |
+---------------+-------------------+---------------------------+
|   Multiply    |   O(nk)           |   O(nk)                   |
+---------------+-------------------+---------------------------+
|   x in s      |   O(n)            |   O(n)                    |
+---------------+-------------------+---------------------------+
|min(s), max(s) |   O(n)            |   O(n)                    |
+---------------+-------------------+---------------------------+
|   Get Length  |   O(1)            |   O(1)                    |
+---------------+-------------------+---------------------------+

*k* oznacza wielkość wycinka.


Wbudowane algorytmy - słownik
-----------------------------

+---------------+-------------------+---------------------------+
|   Operation   |   Average Case    |   Amortized Worst Case    |
+===============+===================+===========================+
|   Copy[2]     |   O(n)            |   O(n)                    |
+---------------+-------------------+---------------------------+
|   Get Item    |   O(1)            |   O(n)                    |
+---------------+-------------------+---------------------------+
|   Set Item[1] |   O(1)            |   O(n)                    |
+---------------+-------------------+---------------------------+
|   Delete Item |   O(1)            |   O(n)                    |
+---------------+-------------------+---------------------------+
|  Iteration[2] |   O(n)            |   O(n)                    |
+---------------+-------------------+---------------------------+

[1] = Pojedyncza operacja może trwać długo, w zależności od poprzednich
wartości słownika

[2] = Dla tych operacji *n* oznacza największą pojemność jaką kiedykolwiek
osiągnął słownik, a nie bieżącą.

Wbudowane algorytmy - zbiór
---------------------------

+------------------------+-----------------------+--------------------+
|   Operation            |   Average Case        |Amortized Worst Case|
+========================+=======================+====================+
|   x in s               |   O(1)                |   O(n)             |
+------------------------+-----------------------+--------------------+
|   Union s|t            |  O(len(s)+len(t))     |  O(len(s)+len(t))  |
+------------------------+-----------------------+--------------------+
| Intersection s&t       | O(min(len(s), len(t)) | O(len(s) * len(t)) |
+------------------------+-----------------------+--------------------+
| Difference s-t         |   O(len(s))           |  O(len(s))         |
+------------------------+-----------------------+--------------------+
| s.difference_update(t) |  O(len(t))            |  O(len(t))         |
+------------------------+-----------------------+--------------------+

Moduł collections
=================

Opis modułu
-----------

Moduł *collections* zawiera wyspecjalizowane typy danych.
Są one alternatywą dla wbudowanych typów danych takich jak *dict*, *list*, *set* i *tuple*.

+---------------+-------------------------------------------+-------------+
| **nazwa typu**| **opis**                                  | **dostępny  |
|               |                                           | od Pythona**|
+===============+===========================================+=============+
| namedtuple()  | funkcja służąca do tworzenia krotek       | 2.6         |
|               | z nazwanymi polami                        |             |
+---------------+-------------------------------------------+-------------+
| deque         | kontener podobny do listy, z dostępem     | 2.4         |
|               | zarówno od początku jak i od końca        |             |
+---------------+-------------------------------------------+-------------+
| Counter       | klasa słownika służąca do liczenia        | 2.7         |
|               | obiektów                                  |             |
+---------------+-------------------------------------------+-------------+
| OrderedDict   | klasa słownika, która pamięta kolejność,  | 2.7         |
|               | w jakiej dodawano elementy                |             |
+---------------+-------------------------------------------+-------------+
| defaultdict   | klasa słownika, która  umożliwia          | 2.5         |
|               | zdefiniowanie brakujących elementów       |             |
+---------------+-------------------------------------------+-------------+

Namedtuple
----------

Nazwane krotki są stosowane wszędzie tam, gdzie potrzebujemy manipulować niewielkimi rekordami, a jednocześnie nie chcemy tworzyć osobnej klasy.
Przykładem jest klasa ``Point``:

.. code-block:: python

    from collections import namedtuple

    Point = namedtuple('Point', 'x y z')

    def distance(a, b):
        return math.sqrt(
            (a.x - b.x)**2 +
            (a.y - b.y)**2 +
            (a.z - b.z)**2
        )

.. code-block:: pycon

    >>> a = Point(x=1, y=2, z=3)
    >>> b = Point(-1, -2, 42)
    >>> print(distance(a, b))
    39.25557285278104

Nazwane krotki przydają się także wtedy, gdy chcemy zwrócić więcej niż jeden obiekt.
Funkcja lub metoda może zwrócić kilka obiektów w krotce, ale trzeba wówczas pamiętać, w jakiej kolejności są one zwracane.
Alternatywą jest zwrócenie nazwanej krotki:

.. code-block:: python

    from collections import namedtuple

    Result = namedtuple('Result', 'quotient remainder')

    def divmod(a, b):
        return Result(quotient=a//b, remainder=a%b)

.. code-block:: pycon

    >>> r = divmod(9, 2)
    >>> r
    Result(quotient=4, remainder=1)
    >>> r[0]
    4
    >>> r.quotient
    4

Deque
-----

W niektórych algorytmach stosowane są kolejki FIFO (*first in, first out*).
Kolejka jest listą, której nowe elementy są dodawane na jej początku.
Z kolei inny wątek może pobierać elementy (tzn. odczytywać je i usuwać z listy) z jej końca.

Stosowanie listy może spowolnić program, ponieważ dodanie lub usunięcie elementu z początku listy jest operacją kosztowną (złożoność *O(n)*).
Dlatego w takich sytuacjach stosuje się wyspecjalizowany kontener ``deque``.
Jego zaletą jest możliwość szybkiego (tzn. w czasie stałym *O(1)*) dodawania i usuwania elementów zarówno z początku jak i końca.

`deque` udostępnia ten sam interfejs, co listy.
Ponadto, dostępne są metody ``appendleft``, ``extendleft`` oraz ``popleft``, które działają tak samo jak odpowiednio ``append``, ``extend`` i ``pop``, ale działają na początku kolekcji zamiast na jej końcu.

Na przykład, ``appendleft(x)`` dodaje obiekt *x* na początek kolejki.
Jest to równoważne wywołaniu ``insert(0, x))``.

Przykład użycia ``deque``:

.. code-block:: pycon

    >>> import collections

    >>> d = collections.deque('abcdefg')
    >>> d
    deque(['a', 'b', 'c', 'd', 'e', 'f', 'g'])
    >>> len(d)
    7
    >>> d[0]
    a
    >>> d[-1]
    g

    >>> d.remove('c')
    >>> d
    deque(['a', 'b', 'd', 'e', 'f', 'g'])
    >>> d = collections.deque('abcdefg')
    >>> while True:
    ...     try:
    ...         print(d.pop())
    ...     except IndexError:
    ...         break
    ...
    g
    f
    e
    d
    c
    b
    a

    >>> d = collections.deque('abcdefg')
    >>> while True:
    ...     try:
    ...         print(d.popleft())
    ...     except IndexError:
    ...         break
    ...
    a
    b
    c
    d
    e
    f
    g
    >>> d = collections.deque(range(10))
    deque([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

    >>> d = collections.deque(range(10))
    >>> d.rotate(2)
    deque([8, 9, 0, 1, 2, 3, 4, 5, 6, 7])
    >>> d = collections.deque(range(10))
    >>> d.rotate(-2)
    deque([2, 3, 4, 5, 6, 7, 8, 9, 0, 1])

Counter
-------

``Counter`` jest klasą ułatwiającą zliczanie elementów.
Jest to słownik, którego kluczami są elementy, a wartościami częstość ich występowania.
Przy tworzeniu instancji należy przekazać kolekcję elementów, które mają zostać zliczone.

Zliczanie znaków występujących w danym napisie można wykonać przy pomocy zwykłego słownika:

.. code-block:: python

    counter = {}
    for char in "Hello there, People!":
        if char not in counter:
            counter[char] = 1
        else:
            counter[char] += 1
    print(counter)

.. code-block:: pycon

    {'a': 1, ' ': 2, 'C': 1, 'e': 4, 'd': 1, 'H': 1, 'M': 1,
    'l': 2, 'o': 2, ',': 1, 's': 1, 'r': 1, '!': 1, 't': 1, 'h': 2}

Lub szybciej, przy pomocy klasy ``Counter``:

.. code-block:: python

    from collections import Counter
    counter = Counter("Hello there, People!")
    print(counter)

.. code-block:: python

    Counter({'e': 4, ' ': 2, 'l': 2, 'o': 2, 'h': 2, 'a': 1,
    'C': 1, 'd': 1, 'H': 1, 'M': 1, ',': 1, 's': 1, 'r': 1, '!': 1, 't': 1})

``most_common`` jest metodą umożliwiającą sprawdzenie, które elementy występują najczęściej.
Jeśli chcemy znaleźć najczęściej występujące słowa w pliku ``hamlet.txt``, możemy wykonać:

.. code-block:: python

    import re
    words = re.findall('\w+', open('hamlet.txt').read().lower())
    print(Counter(words).most_common(10))

.. code-block:: pycon

    [('the', 1143), ('and', 966), ('to', 762), ('of', 669), ('i', 631),
     ('you', 554),  ('a', 546), ('my', 514), ('hamlet', 471), ('in', 451)]

OrderedDict
-----------

Słowniki przechowują pary *(klucz, wartość)*, ale nie pamiętają kolejności, w jakiej poszczególne pary zostały dodane.
``OrderedDict`` jest słownikiem, który zapamiętuje tę kolejność.
Podczas iterowania po nim, zwraca on klucze w kolejności, w jakiej zostały dodane:

.. code-block:: pycon

    >>> from collections import OrderedDict
    >>> d = OrderedDict()
    >>> d['c'] = 1
    >>> d['b'] = 2
    >>> d['a'] = 1
    >>> for k in d:
    ...     print(k)
    ...
    c
    b
    a

Dodatkowo, ten obiekt udostępnia metodę ``popitem``:

.. code-block:: pycon

    >>> d.popitem()  # zwraca ostanio dodaną parę (klucz, wartość) (kolejka LIFO)
    ('a', 1)
    >>> d.popitem(last=False)  # zwraca pierwszą dodaną parę (kolejka FIFO)
    ('c', 1)

defaultdict
-----------

``defaultdict`` jest takim słownikiem, w którym użycie klucza nie będącego w tym słowniku powoduje użycie domyślnej wartości zamiast zgłoszenia wyjątku ``KeyError``.

Ten obiekt pozwala uprościć kod wykorzystujący ideę multisłownika, tzn. słownika, w którym jeden klucz może mieć więcej niż jedną wartość.
Taki multisłownik może zostać zaimplementowany jako zwykły słownik, którego wartościami jest lista.

.. code-block:: python

    s = [('yellow', 1), ('blue', 2), ('yellow', 3), ('blue', 4), ('red', 1)]
    res = {}
    for color, item in s:
        if color in res:
            res[color].append(item)
        else:
            res[color] = [item, ]

    res.items()

.. code-block:: pycon

    [('blue', [2, 4]), ('red', [1]), ('yellow', [1, 3])]

Rozwiązanie z użyciem ``defaultdict`` jest znacznie prostsze.
Przy tworzeniu słownika przekazujemy funkcję, która generuje wartość domyślną.
W tym przypadku wartością domyślną jest pusta lista, którą generuje wbudowana funkcja ``list``.

.. code-block:: python

    from collections import defaultdict

    s = [('yellow', 1), ('blue', 2), ('yellow', 3), ('blue', 4), ('red', 1)]
    d = defaultdict(list)
    for k, v in s:
        d[k].append(v)

    print(d)

.. code-block:: pycon

    defaultdict(list, {'blue': [2, 4], 'red': [1], 'yellow': [1, 3]})

Omówienie idiomów gwarantujących większą wydajność kodu
=======================================================

Konkatenacja ciągów
-------------------

W przypadku łączenia mniejszych stringów w dłuższy ciąg znaków, należy unikać poniższej implementacji:

.. code-block:: python

    list = [str(i) for i in range(100000)]
    joined = ""
    for substring in list:
        joined += substring

Konkatenacja napisu na końcu innego napisu (``s += substring``) jest bardzo kosztowna obliczeniowo.
Wykonywanie tej operacji w pętli może znacząco spowolnić program.

Dlatego lepiej jest użyć metody ``join``:

.. code-block:: python

    joined = ''.join(list)

Jako argument można przekazać także wyrażenie listowe, co jest przydatne, gdy napisy są generowane.

.. code-block:: python

    strings = [some_function(elem) for elem in somelist]
    joined = "".join(strings)

Podobnie, zamiast ręcznego łączenia stringów przy pomocy operatora ``+``:

.. code-block:: python

    head = "www.site.pl"
    query = "&q=costam"
    tail = "&u=ktos"
    url = "http://" + head + query + tail + "/"

lepiej jest użyć wbudowanej funkcji ``format``:

.. code-block:: python

    url = "http://{}{}{}/".format(head, query, tail)

Sortowanie
----------

Sortowanie list jest wydajne w Pythonie, niestety użycie funkcji porównującej znacznie spowalnia proces.
Aby tego uniknąć, można zastosować tzw. *"Schwartzian Transform"*, czyli utworzenie listy krotek, której pierwszy element to klucz sortowania:

.. code-block:: python

    somelist = [('John', 'Smith', 21), ('Alice', 'Johnson', 24)]
    n = 1  # sortowanie po drugim polu krotki, tj. po nazwisku
    nlist = [(x[n], x) for x in somelist]
    nlist.sort()
    somelist = [val for (key, val) in nlist]

Krotki są porównywalne, a zatem można je także sortować.
Z dwóch krotek ta jest mniejsza, której pierwszy element jest mniejszy:

.. code-block:: pycon

    >>> (2, 42) < (3, 42)
    True
    >>> ('a', 42) < ('z', 42)
    True

Jeżeli pierwszy element jest taki sam, wówczas porównywany jest drugi element:

    >>> ('to samo', 2) < ('to samo', 20)
    True

Jeżeli drugi element również jest taki sam, wówczas porównywany jest trzeci element itd.

Od Pythona 2.4 i w 3.x możemy podać "klucz" według którego nastąpi sortowanie:

.. code-block:: python

    import operator
    nlist.sort(key=operator.itemgetter(n))

Przykład porównujący czas sortowania:

.. code-block:: python

    from random import randint
    nlist = [('bla', randint(0, 10000)) for _ in range(10000)]
    import operator
    %timeit sorted(nlist, key=operator.itemgetter(1))
    %timeit sorted([(x[1], x) for x in nlist])

.. code-block:: pycon

    8.13 ms ± 211 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)
    16 ms ± 206 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)

Pętle
-----

Zobaczmy, jak wygląda przykładowa pętla pracująca na liście słów:

.. code-block:: python

    newlist = []
    for word in oldlist:
        newlist.append(word.upper())

W takim przypadku należy pamiętać, że pętle to nie tylko *for*.

Można użyć wbudowanej funkcji ``map``.

.. code-block:: python

    newlist = map(str.upper, oldlist)

Należy jednak unikać używania funkcji ``map`` i ``filter``, ponieważ wymaga to wywołania funkcji dla każdego elementu kolekcji, co jest dosyć kosztowne.
Dlatego lepiej jest użyć wyrażeń listowych:

.. code-block:: python

    newlist = [s.upper() for s in oldlist]

lub generatorowych:

.. code-block:: python

    new_iterator = (s.upper() for s in oldlist)

Przykład ilustrujący czas wykonania poszczególnych operacji:

.. code-block:: python

    strlist = ['asdf' for _ in range(100000)]

.. code-block:: python

    %%timeit
    newlist = []
    for word in strlist:
        newlist.append(word.upper())

.. code-block:: pycon

    42.1 ms ± 1.22 ms per loop (mean ± std. dev. of 7 runs, 10 loops each)

.. code-block:: python

    %timeit list(map(lambda s: s.upper(), strlist))
    %timeit list(map(str.upper, strlist))
    %timeit [s.upper() for s in strlist]

.. code-block:: pycon

    45.7 ms ± 2.48 ms per loop (mean ± std. dev. of 7 runs, 10 loops each)
    32.2 ms ± 1.76 ms per loop (mean ± std. dev. of 7 runs, 10 loops each)
    28.7 ms ± 1.14 ms per loop (mean ± std. dev. of 7 runs, 10 loops each)

Unikanie przeszukiwania
-----------------------

Jeśli nie możemy zrezygnować z pętli *for* należy ograniczyć użycie operatora ``.``.

Z drugiej strony, należy pamiętać, iż zaciemnia to kod, dlatego ta technika powinna być stosowana tylko w sytuacji, gdy program wykonuje się za wolno.
Nie ma sensu stosowanie jej "na wszelki wypadek".

.. code-block:: python

    upper = str.upper
    newlist = []
    append = newlist.append
    for word in oldlist:
        append(upper(word))

Przykład ilustrujący czas wykonania poszczególnych operacji:

.. code-block:: python

    %%timeit
    newlist = []
    for word in strlist:
        newlist.append(word.upper())

.. code-block:: pycon

    41.4 ms ± 1.47 ms per loop (mean ± std. dev. of 7 runs, 10 loops each)

.. code-block:: python

    %%timeit
    upper = str.upper
    newlist = []
    append = newlist.append
    for word in strlist:
        append(upper(word))

.. code-block:: pycon

    38.9 ms ± 895 µs per loop (mean ± std. dev. of 7 runs, 10 loops each)

Lokalne vs. globalne
--------------------

Czas dostępu do zmiennych lokalnych jest o wiele krótszy. Dlatego poniższy kod:

.. code-block:: python

    def upper_words(oldlist):
        upper = str.upper
        newlist = []
        append = newlist.append
        for word in oldlist:
            append(upper(word))
        return newlist

jest szybszy niż:

.. code-block:: python

    upper = str.upper
    def upper_words(oldlist):
        newlist = []
        append = newlist.append
        for word in oldlist:
            append(upper(word))
        return newlist

Słowniki
--------

Częstym motywem podczas pracy ze słownikami jest konieczność sprawdzenia, czy dany klucz istnieje, zanim powiązana wartość zostanie zmieniona.
Na przykład, budowanie słownika częstotliwości występowania słów wygląda następująco:

.. code-block:: python

    words = ['a' for _ in range(100000)]

.. code-block:: python

    %%timeit
    wdict = {}
    for word in words:
        if word not in wdict:
            wdict[word] = 0
        wdict[word] += 1

.. code-block:: pycon

    23.2 ms ± 84.9 µs per loop (mean ± std. dev. of 7 runs, 10 loops each)

Jeżeli słowo występuje w słowniku, to taki kod przeszukuje słownik dwukrotnie - raz w *if* i na końcu pętli, podczas inkrementacji wartości.
Aby tego uniknąć, można użyć wyjątków:

.. code-block:: python

    %%timeit
    counter = {}
    for word in words:
        try:
            counter[word] += 1
        except KeyError:
            counter[word] = 1

.. code-block:: pycon

    19.9 ms ± 487 µs per loop (mean ± std. dev. of 7 runs, 10 loops each)

Alternatywnie, można też użyć metody ``get``:

.. code-block:: python

    get = counter.get
    for word in words:
        counter[word] = get(word, 0) + 1

Jednak w powyższym przypadku najlepiej będzie użyć ``defaultdict``:

.. code-block:: python

    %%timeit
    from collections import defaultdict

    counter = defaultdict(int)
    for word in words:
        counter[word] += 1

.. code-block:: pycon

    18.6 ms ± 265 µs per loop (mean ± std. dev. of 7 runs, 10 loops each)

Agregacja danych
----------------

Python ma dość duży narzut na wywołanie funkcji i należy tego unikać, zwłaszcza w pętlach:

.. code-block:: python

    counter = 0
    def count(i):
        global counter
        counter = counter + 1

    l = list(range(100000))
    %timeit for i in l: count(i)

.. code-block:: pycon

    34.3 ms ± 674 µs per loop (mean ± std. dev. of 7 runs, 10 loops each)

Znacznie szybsze będzie jednokrotne wywołanie funkcji z przekazaniem całej kolekcji elementów:

.. code-block:: python

    counter = 0
    def fast_count(list):
        global counter
        for i in list:
            counter = counter + 1
    %timeit fast_count(l)

.. code-block:: pycon

    18 ms ± 654 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)

Sloty
-----

Jeżeli program wykorzystuje dużo pamięci RAM i wynika to z tworzenia milionów małych obiektów, wtedy warto rozważyć dodanie do ich klasy atrybutu ``__slots__``.
Domyślnie, każdy obiekt posiada słownik swoich atrybutów, co generuje spory narzut na pamięć.
Jeśli nasza klasa nie korzysta z dynamicznej natury takiego słownika (tzn. nie dodaje dynamicznie nowych atrybutów), to można wyliczyć wszystkie atrybuty w ``__slots__``.
Obiekty takiej klasy będą zachowywać się podobnie do struktury z C.

.. code-block:: python

    class Point(object):
        __slots__ = ["x", "y"]

.. code-block:: pycon

    >>> p = Point()
    >>> p.x = 10
    >>> p.y = 20
    >>> p.z = 30
    Traceback (most recent call last):
      File "<pyshell#54>", line 1, in <module>
        p.z = 30
    AttributeError: 'Point' object has no attribute 'z'
