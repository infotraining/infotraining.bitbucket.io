***************************
Object oriented programming
***************************

Introduction
============

Python is built around the concept of **object**. Every piece of data (lists, dictionaries, functions, modules, etc.) stored and used by Python is an object.

Each object has:
    * *Identity* - indicates the location of the object in memory
    * *Type* - describes the object representation for Python
    * *Value* - data stored in the object

::

    >>> numbers = [1, 2, 3]
    >>> id(numbers)
    30098576
    >>> type(numbers)
    <type 'list'>
    >>> numbers
    [1, 2, 3]

Once an object is created, its identity and type cannot be changed.
If the value of an object can be changed, it is a *mutable* object.
If it cannot be changed - *immutable* object.
For example, the types ``str`` and ``tuple`` are immutable.

::

    >>> tuple = (1, 2, 3)
    >>> tuple[0] = 2
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    TypeError: 'tuple' object does not support item assignment

Some objects have:
    * Attributes - values ​​associated with the object
    * Methods - callable functions that operate on the object

Attributes and methods are accessed by using the dotted ``.`` syntax ::

    >>> stream = open(r"c:\\Training\\test.txt", "w")
    >>> print(stream.closed)
    False
    >>> stream.close()
    >>> print(stream.closed)
    True

Classes
=======

.. index ::
   single: class

Classes are collections of attributes and methods.

Classes allow you to:
    * Create new user-defined data types
    * Extending the capabilities of existing data types

Class definition
----------------

.. code-block:: python

    class BankAccount:
        def __init__(self, owner, balance = 0):
            self.owner = owner
            self.balance = balance

        def withdraw(self, amount):
            self.balance -= amount

        def deposit(self, amount):
            self.balance += amount

        def info(self):
            print("owner:", self.owner, "; balance:", self.balance)

.. warning::
    In Python 2.x, the class definition should contain the parent ``class BankAccount(object):``.
    Otherwise, the so-called "old-type" class will be generated. 
    All classes in Python 3.x are "new type".

Object creation
---------------

To create an object of a given class, the class should be called with the operator ``()``.

Example:

.. code-block:: python

    jk = BankAccount("John Doe", 1000)
    jk.info()
    jk.deposit(2000)
    jk.withdraw(2500)
    jk.info()
    jk.balance = 0 # Access to the balance member
    jk.info()

::

    owner: John Doe; balance: 1000
    owner: John Doe; balance: 500
    owner: John Doe; balance: 0

Private members 
---------------

.. index::
   single: private members

All attributes and methods defined in the class are public.
To hide an attribute or method from access from outside of the class (private member), precede its name with two underscores (e.g. ``__attribute``).

.. code-block:: python

    class BankAccount:
        def __init__(self, owner, balance = 0):
            self.owner = owner
            self.__balance = balance

        def withdraw(self, amount):
            self.__balance -= amount

        def deposit(self, amount):
            self.__balance += amount

        def info(self):
            print ("owner:", self.owner, "; balance:", self.__balance)

    account = BankAccount("Kowalski", 1000)
    print(account.balance) # Error!
    print(account._BankAccount__balance) # OK

Static members
--------------

.. index::
   single: static members

Static members are common to all class instances.

.. code-block:: python

    class CountedObject:
        __count = 0      # Static member

        def __init__(self):
            CountedObject.__count += 1


        @staticmethod
        def staticGetCount():
            return CountedObject.__count

        @classmethod
        def classGetCount(cls):
            print ("classGetCount invoked for instance of", cls)
            return cls.__count

    print ("Number of objects: %s" % CountedObject.staticGetCount())
    print ("Creating objects ...")

    first_counted_object = CountedObject()
    second_counted_object = CountedObject()
    more_counted_objects = [CountedObject(), CountedObject()]

    print ("Number of objects:% s"% CountedObject.staticGetCount())
    print ("Number of objects:% s"% CountedObject.classGetCount())

::

    Number of objects: 0
    Creating objects ...
    Number of objects: 4
    classGetCount invoked for instance of <class '__main __. CountedObject'>

Properties
----------

.. index::
   single: properties

Properties enable an object to be encapsulated.
They are the equivalent of access methods.

.. code-block:: python

    class Rectangle:
        def __init__(self):
            self.width = 0
            self.height = 0
        def setSize(self, size):
            self.width, self.height = size
        def getSize(self):
            return self.width, self.height
        size = property(getSize, setSize)

::

    >>> r = Rectangle ()
    >>> r.width = 10
    >>> r.height = 20
    >>> r.size
    (10, 20)
    >>> r.size = 150,100
    >>> r.height
    100

It is possible to define "read-only" type properties.

.. code-block:: python

    class BankAccount:
        counter = 0
        def __init__(self, owner, balance = 0):
            self.owner = owner
            self.__balance = balance
            BankAccount.counter + = 1
        def __getBalance(self):
            return self.__balance
        balance = property(__getBalance)

::

    >>> account = BankAccount("jk", 100)
    >>> account.balance
    100
    >>> account.balance = 100 # Error!


Special attributes
------------------

.. index ::
   single: special attributes

Class instances have special attributes that describe the objects. ::

    >>> account = BankAccount("Kowalski", 1000)
    >>> account.__dict__ # Dictionary of user-defined attributes
    {'owner': 'Kowalski', '_BankAccount__balance': 1000}
    >>> account.__class__.__name__ # Name of the class
    'BankAccount'
    >>> account.withdraw.__name__ # Name of the method
    'withdraw'
    >>> [attrib for attrib in dir (account) if not attrib.startswith ('_')] # Interface
    ['counter', 'deposit', 'info', 'owner', 'withdraw']

Special methods
----------------

.. index::
   single: special methods

Classes allow operators to be overloaded (similar to C++).
To achieve the overloading of operators, special methods must be implemented in the class.

.. code-block:: python

    class Special:
        def __init__(self, *args, **kwargs):
            pass
            # constructor

        def __del__(self):
            pass
            # Destructor - rarely used

        def __str__(self):
            pass
            # Character representation; called by print and str

        def __repr__(self):
            pass
            # Character representation; triggered by repr
            # eval (repr (a)) should be a

        def __getitem__(self, i):
            pass
            # Indexing for the object: b = a [i]

        def __setitem__(self, i, v):
            pass
            # Assignment using indexation: a [i] = v

        def __len__(self):
            pass
            # Fired by len (a); The function should return
            # object length (if justified)

        def __eq__(self, x):
            pass
            # Test self == x; returns True or False

        def __add__(self, b):
            pass
            # Defines self + b

        def __sub__(self, b):
            pass
            # Defines self - b

        def __mul__(self, b):
            pass
            # Defines self * b

        def __div__(self, b):
            pass
            # Defines self / b

        def __pow__(self, b):
            pass
            # Defines self ** b


Inheritance
===========

.. index::
   single: inheritance

Inheritance - allows you to create new classes that inherit the form and functionality of the base class.

Inheritance is defined using the syntax:

.. code-block:: python

    class Base:
        pass
    class Derived(Base):
        pass

Example:

.. code-block:: python

    class CheckedAccount(BankAccount):
        def __init__(self, owner, balance=0, limit=0):
            BankAccount.__init__(self, owner, balance)
            self.limit = limit

        def withdraw(self, amount):
            if (self.balance-amount) <= self.limit:
                print("Insufficient funds")
            else:
                BankAccount.withdraw(self, amount)

To call the base class constructor, you can use the ``super()`` method returning the parent's *proxy* object.

.. code-block:: python

    def __init__(self, owner, balance=0, limit=0):
        super().__init__(owner, balance)
        self.limit = limit


Multiple inheritance
--------------------

.. index::
   single: multiple inheritance

Multiple inheritance - classes can inherit from several base classes.

.. code-block:: python

    class CountedObject:
        __count = 0       # Static member

        def __init__(self, *args, **kwargs):
            CountedObject.__ count += 1
            super().__init__(* args, ** kwargs)

        @staticmethod
        def get_count():
            return CountedObject.__count

    class Calculator:
        def repl(self):
            while True:
                expr = self.read()
                value = self.evaluate(expr)
                self.print(value)

        def read(self):
            return input()

        def evaluate(self, expr):
            return eval(expr)

        def print(self, value):
            print(value)

    class CountedCalculator(CountedObject, Calculator):
        def read (self):
            return input ('>')

::

    >>> d = CountedCalculator()
    >>> d.get_count()
    1
    > 2 + 2
    4
    >

Interfaces and introspection
----------------------------

It is possible to check the characteristics of classes and objects while the program is running.

::

    >>> issubclass(CountedCalculator, CountedObject)
    True
    >>> issubclass(Calculator, CountedCalculator)
    False
    >>> CountedCalculator.__bases__
    (__main__.CountedObject, __main__.Calculator)
    >>> d = CountedCalculator()
    >>> isinstance(d, Calculator)
    True