***********************************
Nowe elementy dla autorów bibliotek
***********************************

Statyczne asercje - ``static_assert``
=====================================

Nowe słowo kluczowe ``static_assert`` umożliwia stosowanie asercji, które są ewaluowane na etapie kompilacji.

Format: ``static_assert(condition, msg)``

* jeśli warunek ``condition`` nie jest spełniony, generuje błąd kompilacji z komunikatem ``msg``
* ``msg`` musi być literałem znakowym

``static_assert`` w zasięgu klasy
---------------------------------

Najczęściej statyczne asercje są używane w kodzie szablonowym:

.. code::

    template <typename T>
    class OnlyCompatibleWithIntegralTypes
    {
       static_assert(std::is_integral<T>::value, "T must be integral");
    };

    OnlyCompatibleWithIntegralTypes<double> test; // błąd kompilacji!


``static_assert`` w zasięgu funkcji
-----------------------------------

Asercji statycznych można używać również w funkcjach lub szablonach funkcji.

.. code:: cpp

    template <typename T, size_t N>
    void accepts_arrays_with_size_between_1_and_255(T (&arr)[N])
    {
        static_assert((N >= 1) && (N <= 255), "size of array must be in range [1, 255]");
    }


Uogólnione stałe wyrażenia - ``constexpr``
==========================================

C++11 wprowadza dwa znaczenia dla "stałej":

* ``constexpr`` - stała ewaluowana na etapie kompilacji
* ``const`` - stała, której wartość nie może ulec zmianie

Wyrażenie stałe (*constant expression*) jest wyrażeniem ewaluowanym przez kompilator na etapie kompilacji. Nie może zawierać wartości, które nie są znane na etapie kompilacji i nie może mieć efektów ubocznych.

Jeśli wyrażenie inicjalizujące dla ``constexpr`` nie będzie mogło być wyliczone na etapie kompilacji kompilator zgłosi błąd:

.. code::

    int x1 = 7;
    constexpr int x2 = 7;

    constexpr x3 = x1; // error: initializer is not a contant expression
    constexpr x4 = x2; // OK

W wyrażeniu ``contexpr`` można użyć:

* Wartości typów całkowitych, zmiennoprzecinkowych oraz wyliczeniowych
* Operatorów nie modyfikujących stanu (np. +, ? i [] ale nie = lub ++)
* Funkcji ``constexpr``
* Typów literalnych
* Stałych ``const`` zainicjowanych stałym wyrażeniem

.. code::

    constexpr int factorial(int n)
    {
        return (n == 0) ? 1 : n * factorial(n-1);
    }

    template <typename T, size_t N>
    constexpr size_t size_of_array(T (&)[N])
    {
        return N;
    }

    //...

    const int SIZE = 2;

    int arr1[factorial(1)];
    int arr2[factorial(SIZE)];
    int arr3[factorial(3)];
    int arr4[factorial(size_of_array(arr3))];

Zmienne ``constexpr``
---------------------

W C++11 ``constexpr`` przed zmienną definiuje ją jako stałą, która musi zostać zainicjowana wyrażeniem stałym.

.. important:: Stała ``const`` w odróżnieniu od stałej ``constexpr`` nie musi być zainicjowana wyrażeniem stałym.

.. code::

    constexpr int x = 7;

    constexpr auto prefix = "Data";

    constexpr int n_x = factorial(x);

    constexpr double pi = 3.1415;

    constexpr double pi_2 = pi / 2;

Funkcje ``constexpr``
---------------------

Funkcje mogą zostać zadeklarowane jako ``constexpr`` jeśli spełniają dwa wymagania:

* Ciało funkcji zawiera tylko jedną instrukcję ``return`` zwracającą wartość, która nie jest typu ``void``
* Typ wartości zwracanej oraz typy parametrów powinny być typami dozwolonymi dla wyrażeń ``constexpr``


Typy literalne
--------------

C++11 wprowadza pojęcie **typu literalnego** (*literal type*), który może być użyty w stałym wyrażeniu ``constexpr``:

Typem literalnym jest:

* Typ arytmetyczny (całkowity, zmiennoprzecinkowy, znakowy lub logiczny)
* Typ referencyjny do typu literalnego (np: ``int&``, ``double&``)
* Tablica typów literalnych
* Klasa, która:

  - ma trywialny destruktor
  - wszystkie niestatyczne składowe i typy bazowe są typami literalnymi
  - jest agregatem lub ma przynajmniej jeden konstruktor ``contexpr``, który nie jest konstruktorem kopiującym lub przenoszącym (konstruktor musi mieć
    pustą implementację, ale umożliwia inicjalizację składowych na liście inicjalizującej)


.. code:: cpp

    class Complex
    {
        double real_, imaginary_;
    public:
        constexpr Complex(const double& real, const double& imaginary)
         : real_ {real}, imaginary_ {imaginary}
        {}

        constexpr double real() const { return real_; };
        constexpr double imaginary() const { return imaginary_; }
    };

    //...

    constexpr Complex c1 {1, 2};


Agregaty, typy POD i Standard Layout
====================================

Agregaty w C++11
----------------

**Agregat** (*aggregate*) - tablica lub klasa, która jest agregatem nie może mieć

* Konstruktora dostarczonego przez użytkownika (*user-provided constructor*)
* Niestatycznych składowych zainicjowanych w miejscu deklaracji
* Prywatnych lub chronionych niestatycznych pól składowych
* Klas bazowych
* Metod wirtualnych

.. code::

    class NotAggregate1
    {
        virtual void f(){} // no virtual functions allowed
    };

    class NotAggregate2
    {
        int x; // x is private by default and non-static
    };

    class NotAggregate3
    {
    public:
        NotAggregate3(int) {} // user-provided constructor
    };

    class Aggregate1
    {
    public:
      NotAggregate1 member1;   // ok, public member
      Aggregate1& operator=(const Aggregate1& rhs) {/* */} // OK, copy-assignment
    private:
      void f() {} // OK, just a private function
    };

    class Aggregate2
    {
    public:
        Aggregate2() = default; // OK, user-declared but not user-provided
    }

Agregaty mogą być inicjalizowane nawiasami klamrowymi (nie należy mylić z inicjalizacją przy pomocy konstruktora z ``std::initializer_list<>``).
Jeśli elementów na liście jest mniej niż w deklaracji agregatu, pozostałe elementy są w miarę możliwości inicjalizowane wartościowo (tzn. typy wbudowane są zerowane a dla typów użytkownika wywoływany jest konstruktor domyślny)

.. code:: cpp

    struct Aggregate1
    {
        int a;
        double b;
        vector<int> vec;

        Aggregate1() = default;

        void print(const string& prefix) const
        {
            cout << prefix << ": a=" << a << "; b=" << b;
            cout << "; vec=[ ";
            for(auto& item : vec)
                cout << item << " ";
            cout << "]" << endl;
        }
    };

    //...
    Aggregate1 agg1 = {1, 4.5, {3, 4, 5, 6}};

    agg1.print("agg1");


Typy POD
--------

Idea typu **POD** (*Plain-Old Data*) została wprowadzona w C++98 aby dać wsparcie dla:

1. Możliwości statycznej inicjalizacji typu.
2. Kompilacji typu POD do postaci kompatybilnej bitowo (*memory layout*) ze strukturami języka C

Ponieważ cele te są w zasadzie rozłączne w C++11 rozdzielono je wprowadzając dwa różne koncepty:

* Klasy trywialne - *tirivial classes*
* Klasy *standard-layout*

.. important:: Nowa definicja typu POD w C++11 mówi, że klasa POD to klasa, która jest zarówno trywialna (*trivial*) jak i ma standardowy layout. Właściwość ta musi być rekursywnie spełniona dla wszystkich niestatycznych pól składowych.

Klasy trywialne
---------------

Klasy trywialne (*trivial classes*) wspierają statyczną inicjalizację. Jeśli klasa jest trywialnie kopiowalna może być użyta do kopiowania
bitowego np. ``memcpy()``.

Standard definiuje klasę **trywialnie kopiowalną** (*trivialy copyable*) jako klasę, która:

* nie ma nietrywialnego konstruktora kopiującego
* nie ma nietrywialnego konstruktora przenoszącego
* nie ma nietrywialnego kopiującego operatora przypisania
* nie ma nietrywialnego przenoszącego operatora przypisania
* ma trywialny destruktor

**Klasa trywialna** to klasa, która ma **trywialny konstruktor domyślny** i jest **trywialnie kopiowalna**. W szczególności klasa trywialna
nie ma wirtualnych metod oraz wirtualnych klas bazowych.

Kopiujący/Przenoszący konstruktor jest trywialny, jeśli nie jest zdefiniowany przez użytkownika ``user-provided`` oraz dodatkowo:

* klasa nie posiada wirtualnego konstruktora oraz wirtualnych klas bazowych
* konstruktor kopiujący/przenoszący klasy bazowej jest trywialny
* każde niestatyczne pole składowe ma trywialny konstruktor kopiujący/przenoszący

Podobne wymagania są zdefiniowane dla trywialnego kopiującego/przenoszącego operatora przypisania.

.. code::

    // empty classes are trivial
    struct Trivial1 {};

    // all special members are implicit
    struct Trivial2
    {
        int x;
    };

    struct Trivial3 : Trivial2 // base class is trivial
    {
        Trivial3() = default; // not a user-provided ctor
        int y;
    };

    struct Trivial4
    {
    public:
        int a;
    private: // no restrictions on access modifiers
        int b;
    };

    struct Trivial5
    {
        Trivial1 a;
        Trivial2 b;
        Trivial3 c;
        Trivial4 d;
    };

    struct Trivial6
    {
        Trivial2 a[23];
    };

    struct Trivial7
    {
        Trivial6 c;
        void f(); // it's okay to have non-virtual functions
    };

    struct Trivial8
    {
         int x;
         static NonTrivial1 y; // no restrictions on static members
    }

    struct Trivial9
    {
         Trivial9() = default; // not user-provided

         // a regular constructor is okay because we still have default ctor
         Trivial9(int x) : x(x) {};
         int x;
    }

    struct NonTrivial1 : Trivial 3
    {
        virtual f(); // virtual members make non-trivial ctors
    }

    struct NonTrivial2
    {
        NonTrivial2() : z(42) {} // user-provided ctor
        int z;
    }

    struct NonTrivial3
    {
        NonTrivial3(); // user-provided ctor
        int w;
    }

    NonTrivial3::NonTrivial3() = default; // defaulted but not on first declaration
                                          // still counts as user-provided
    struct NonTrivial5
    {
        virtual ~NonTrivial5(); // virtual destructors are not trivial
    };

Typy standard-layout
--------------------

Typy **standard-layout** mają takie same ułożenie pól w pamięci jak struktury w języku C.

Klasy **standard-layout**:

* nie mają metod wirtualnych i wirtualnych klas bazowych
* wszystkie niestatyczne pola składowe są typu standard-layout lub referencją do takich typów
* wszystkie niestatyczne pola składowe mają taki sam kwalifikator dostępu (``public``, ``protected`` lub ``private``)
* klasa bazowa jest typu standard-layout
* gdy używamy dziedziczenia, tylko jedna klasa w całej hierarchii dziedziczenia może mieć niestatyczne pola składowe i pierwsze
  niestatyczne pole składowe nie może być typu klasy bazowej.

.. code::

    // empty classes have standard-layout
    struct StandardLayout1 {};

    struct StandardLayout2
    {
        int x;
    };

    struct StandardLayout3
    {
    private: // both are private, so it's ok
        int x;
        int y;
    };

    struct StandardLayout4 : StandardLayout1
    {
        int x;
        int y;

        void f(); // perfectly fine to have non-virtual functions
    };

    struct StandardLayout5 : StandardLayout1
    {
        int x;

        StandardLayout1 y; // can have members of base type if they're not the first
    };

    struct StandardLayout6 : StandardLayout1, StandardLayout5
    {
        // can use multiple inheritance as long only
        // one class in the hierarchy has non-static data members
    };

    struct StandardLayout7
    {
        int x;
        int y;

        StandardLayout7(int x, int y) : x(x), y(y) {} // user-provided ctors are ok
    };

    struct StandardLayout8
    {
    public:
        StandardLayout8(int x) : x(x) {} // user-provided ctors are ok
        // ok to have non-static data members and other members with different access
    private:
        int x;
    };

    struct StandardLayout9
    {
        int x;

        static NonStandardLayout1 y; // no restrictions on static members
    };

    struct NonStandardLayout1
    {
        virtual f(); // cannot have virtual functions
    };

    struct NonStandardLayout2
    {
        NonStandardLayout1 X; // has non-standard-layout member
    };

    struct NonStandardLayout3 : StandardLayout1
    {
        StandardLayout1 x; // first member cannot be of the same type as base
    };

    struct NonStandardLayout4 : StandardLayout3
    {
        int z; // more than one class has non-static data members
    };

    struct NonStandardLayout5 : NonStandardLayout3 {}; // has a non-standard-layout


Wersjonowanie bibliotek - ``inline namespace``
==============================================

Przestrzenie nazw ``inline`` ułatwiają wersjonowanie bibliotek i wprowadzanie ich nowych wersji bez potrzeby zmiany kodu po stronie klienta.

Wszystkie symbole umieszczone w przestrzeni nazw ``inline`` są automatycznie dostępne w przestrzeni nazw, która zawiera przestrzeń ``inline``.

.. code:: cpp

    namespace Library
    {
        inline namespace ver_3_2
        {
            double foo();

            void do_something(int);

            template <typename T>
            class Gadget
            {
                // implementation
            };
        }

        namespace ver_3_0
        {
            void do_something(int);

            template <typename T>
            class Gadget
            {
                // implementation
            };
        }

        namespace ver_2_9
        {
            template <typename T>
            class Gadget
            {
                // implementation
            };
        }
    }

Użycie biblioteki ``Library`` wygląda następująco:

.. code:: cpp

    using namespace Library;

    do_something(6);

    auto result1 = foo();
    auto result2 = ver_3_0::foo();
    auto result3 = ver_2_9::foo();

    template <class T>
    Library::Gadget<T*>
    {
        //...
    };


Duplikacja kodu może być uniknięta, jeśli zastosujemy odpowiednio dyrektywy ``#include``.

.. code:: cpp

    // file v_3_common.hpp

    // declarations
    int global;


Plik ``v_3_common.hpp`` zawiera wspólny kod dla różnych wersji bibliotek i może być wielokrotnie włączony przy pomocy ``#include``.


.. code:: cpp

    // file v_3_2.hpp

    namespace v_3_2
    {
        double foo();

        void do_something(int);

        template <typename T>
        class Gadget
        {
            // implementation
        };

        #include "v_3_common.hpp";
    }


.. code:: cpp

    // file v_3_0,hpp

    namespace v_3_0
    {
        #include "v_3_common.hpp";
    }


Plik nagłówkowy biblioteki ``Library``:

.. code:: cpp

    namespace Library
    {
        inline
        #include "v_3_2.hpp";
        #include "v_3_0,hpp";
        #include "v_2_9.hpp";
    }
