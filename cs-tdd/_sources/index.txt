.. TDD w języky C# documentation master file, created by
   sphinx-quickstart on Mon Jul 22 12:50:05 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Test-Driven-Development w języku CSharp
=======================================

Contents:

.. toctree::
   :maxdepth: 2
   
   tdd
   projektowanie_obiektowe_pod_katem_testow
   refaktoryzacja
   testy_jednostkowe
   izolacja_testow
   bdd

