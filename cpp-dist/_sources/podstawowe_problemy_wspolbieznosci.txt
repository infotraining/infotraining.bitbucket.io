**********************************
Podstawowe problemy współbieżności
**********************************

Naruszone niezmienniki
======================

* Współdzielenie danych pomiędzy wątkami może wiązać się z ich modyfikacją
* Niezmiennik— twierdzenie które jest zawsze prawdziwe w określonej sytuacji
* Niezmienniki są często naruszane w przypadku modyfikacji

  * Gdy struktura jest bardziej złożona
  * Wymagana jest modyfikacja więcej niż jednej wartości


Deadlock
========


.. code-block:: c++

  void update_A()
  {
     acquire(A);
     acquire(B); <<< zakleszczenie

     variable++;

     release(B);
     release(A);
  }

  void update_B()
  {
     acquire(B);
     acquire(A); <<< zakleszczenie

     variable++;

     release(A);
     release(B);
  }


Livelock
========

Wątki znajdują się w nieskończonej pętli pobierając i zwalniając blokady.

.. code-block:: c++

  void update1()
  {
     int done=0;

     while (!done)
     {
       acquire(A);

       if ( canAcquire(B) )
       {
         variable1++;
         release(B);
         release(A);
         done=1;
       }
       else
       {
         release(A);
       }
     }
  }

  void update2()
  {
     int done=0;

     while (!done)
     {
       acquire(B);

       if ( canAcquire(A) )
       {
         variable2++;
         release(A);
         release(B);
         done=1;
       }
       else
       {
         release(B);
       }
     }
  }

Zapobieganie zakleszczeniom
---------------------------

Metoda ``boost::lock`` gwarantuje zablokowanie wszystkich muteksów bez zakleszczenia niezależnie od ich kolejności.

.. code-block:: c++

  bool X::operator<(const X& other)
  {
    boost::unique_lock<boost::mutex> l1(the_mutex, boost::defer_lock);
    boost::unique_lock<boost::mutex> l2(other.the_mutex, boost::defer_lock);
    boost::lock(l1, l2);

    return some_data < other.some_data;
  }


Wyścig
======

Simple race
-----------

.. code-block:: c++

  int var;

  void Thread1()
  {
      var++;
  }

  void Thread2()
  {
      var++;
  }

Race on a complex object
------------------------

Kontenery STL nie są bezpieczne wielowątkwo.
Jedynie metody *const* mogą być używane przez wiele wątków.

.. komentarz - ``std::map`` jest oparta na R-B tree

.. code-block:: c++

  std::map<int,int> m;

  void Thread1()
  {
    m[123] = 1;
  }

  void Thread2()
  {
    m[345] = 0;
  }


Thread-hostile reference counting
---------------------------------

Może prowadzić do wycieków pamięci lub podwójnego usuwania.

.. code-block:: c++

  // Ref() and Unref() may be called from several threads.
  // Last Unref() destroys the object.
  class RefCountedObject {
   ...
   public:
    void Ref() {
      ref_++;  // Bug!
    }
    void Unref() {
      if (--ref_ == 0)  // Bug! Need to use atomic decrement!
        delete this;
    }
   private:
    int ref_;
  };

Przykład z projektu Chromium:

.. image:: pics/chromium-Thread-hostile-reference-counting.*

.. chromium używa zmiennych atomowych aby uniknąć tego wyścigu.


Notification
------------

.. generalnie - trudne do wytłumaczenia
.. optymalizacja przez kompilator -> pomija zmienną done jako stałą
.. bo z poziomu pętli jest stałe

.. code-block:: c++

  //Synchronizacji między tymi dwoma wątkami odbywa się za pomocą zmiennej

  bool done = false;

  void Thread1()
  {
    while (!done)
    {
      do_something_useful_in_a_loop_1();
    }
    do_thread1_cleanup();
  }

  void Thread2()
  {
    do_something_useful_2();
    done = true;
    do_thread2_cleanup();
  }


Optymalizacje w trakcie kompilacji:


.. code-block:: c++

  bool done = false;

  void Thread1()
  {
    if (!done)
    {
      while(true) // never exit
      {
        do_something_useful_in_a_loop_1();
      }
    }
    do_thread1_cleanup();
  }

  void Thread2()
  {
    do_something_useful_2();
    done = true;
    do_thread2_cleanup();
  }


Należy zsynchronizować powiadomienie lub użyć barier pamięci


Initializing objects without synchronization
--------------------------------------------

Przykład:

.. problem z ``new``

.. code-block:: c++

  static MyObj *obj = NULL;
  void InitObj() {
    if (!obj)
      obj = new MyObj();
  }
  void Thread1() {
    InitObj();
  }
  void Thread2() {
    InitObj();
  }

Przykład:

.. code-block:: c++

  struct Foo {
    int a;
    Foo() { a = 42; }
  };

  static Foo *foo = NULL;

  void Thread1()  // Create foo.
  {
    foo = new Foo();
  }

  void Thread2()   // Consume foo.
  {
    if (foo)
    {
       assert(foo->a == 42);
    }
  }

Przykład:

.. code-block:: c++

  void Thread2()    // Consume foo.
  {
    Foo *t1 = foo;  // reads NULL
    Foo *t2 = foo;  // reads the new value
    if (t2) {
       assert(t1->a == 42);
    }
  }

Zamiana kolejności
------------------

.. Jeśli ruszają wątki to nie ma pewności czy najpierw A = 1 czy B = 1

.. sources/ordering/gcc

.. code-block:: c++

  int A = 0, B = 0;

  void Thread1() {
    A = 1;
    B = 1;
  }

  void Thread2() {
    if (B == 1) {
       assert(A == 1);
    }
  }


Reader Lock during a write
--------------------------

.. zapis mimo że tryb tylko do odczytu

.. code-block:: c++

  void Thread1() {
    mu.ReaderLock();
    var++;
    mu.ReaderUnlock();
  }
  void Thread2() {
    mu.ReaderLock();
    var++;
    mu.ReaderUnlock();
  }

Double-checked locking
----------------------

.. psuje go - zmiana kolejności przez kompilator
.. mimo iż pointer !null to może być niezainincjowane
.. rozwiązanie - std::call_once, lub barier pamięci

.. code-block:: c++

  bool ready = false;
  void Init()
  {

    // May be called by multiple threads.
    if (!ready)
    {
      mu.Lock();
      if (!ready)
      {
        // .. initialize something
      }

      ready = true;
      mu.Unlock();
    }
  }

.. code-block:: c++

  Singleton* Singleton::instance ()
  {
      Singleton* tmp = pInstance;
      ... // insert memory barrier
      if (tmp == 0)
      {
          Lock lock;
          tmp = pInstance;
          if (tmp == 0)
          {
              tmp = new Singleton;
              ... // insert memory barrier
              pInstance = tmp;
          }
      }
      return tmp;
  }

  // -- c++11
  std::atomic<Singleton*> pInstance;

.. code-block:: c++

  Singleton* Singleton::instance()
  {
    static Singleton _instance;
    return &_instance;
  }

Inicjalizacja statycznych zmiennych jest *threadsafe* w C++11.

http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2660.htm

Volatile
--------

32-bit, brak zabezpieczenia przy ustawianiu zmiennej

.. code-block:: c++

  typedef long long T;

  volatile T vlt64;

  void volatile_store_64(T x)
  {
    vlt64 = x;
  }

Zamiana

.. code-block:: c++

  volatile bool volatile_bool_done;
  double regular_double;

  void volatile_example_2()
  {
    regular_double = 1.23;
    volatile_bool_done = true;
  }


Race during destruction
-----------------------

.. dangling reference

.. code-block:: c++

  void Thread1()
  {
    SomeType object;

    ExecuteCallbackInThread2(
      SomeCallback, &object);
    ...
    // "object" is destroyed when
    // leaving its scope.
  }

Race on free
------------

.. code-block:: c++

  int *array;

  void Thread1()
  {
    array[10]++;
  }

  void Thread2()
  {
    free(array);
  }

* *AddressSanitizer* może wykryć ten błąd


Granulacja
----------

.. code-block:: c++

  class Stack
  {
      int uk;
      int[] stack;
      Mutex mtx;

      void setUp()
      {
        stack = new int[20];
      }

      int getSize()
      {
        Guard( mtx );
        return uk;
      }

      synchronized void push(int x)
      {
        Guard( mtx );
        stack[uk++] = x;
      }

      synchronized int pop()
      {
        Guard( mtx );
        return stack[--uk];
      }
  };

  void thread1()
  {
    for (int i = 0; i < 10; i++)
    {
      push(i);
    }
  }

  void thread2()
  {
    while (getSize() > 0)
    {
      // BAD BAD BAD!
      pop();
    }
  }

Modyfikacja stanu
-----------------

.. code-block:: c++

  class Point
  {
    int x, y;
    Mutex mtx;

    Point()
    {
      x = 0;
      y = 0;
    }

    int getX()
    {
      Guard(mtx);
      return x;
    }

    void setX(int nx)
    {
      Guard(mtx);
      x = nx;
    }

    int getY()
    {
      Guard(mtx);
      return y;
    }

    void setY(int ny)
    {
      Guard(mtx);
      y = ny;
    }

    int[] getXY()
    {
      Guard(mtx);
      return new int[]{x, y};
    }

    void setXY(int[] xy)
    {
      Guard(mtx);
      x = xy[0];
      y = xy[1];
    }
  }

  Point point;

  void setUp()
  {
    point = new Point();
  }

  void thread1()
  {
    point.setX(100);
    // BAD BAD BAD!
    point.setY(100);
  }

  void thread2()
  {
    Point localPoint = new Point();
    int[] xy = point.getXY();
    localPoint.setXY(xy);
  }
