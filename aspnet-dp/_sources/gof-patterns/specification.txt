Specification - specyfikacja
============================

Przeznaczenie
-------------

Wzorzec *Specification* hermetyzuje reguły biznesowe poza klasami encji w postaci obiektów implementujących logikę Boole'a. Pozwala to sprawdzić, czy dany obiekt spełnia założone reguły biznesowe oraz łączyć je ze sobą w czytelny sposób.


Kontekst/Problem
----------------

**Kontekst**

* Kryterium poprawności zdefiniowane w modelu biznesowym nie może być współdzielone lub użyte ponownie przez inne klasy.

**Problem**

* Chcemy oddzielić logikę biznesową, która sprawdza poprawność obiektu od właściwego obiektu.

.. figure:: img/specification.*
    :align: center

    Schemat UML wzorca *Specification*



Implementacja
-------------

Interfejs ``ISpecification<T>``:

.. code-block:: c#

    public interface ISpecification<T>
    {
        bool IsSatisfiedBy(T candidate);
        
        ISpecification<T> And(ISpecification<T> other);

        ISpecification<T> Or(ISpecification<T> other);

        ISpecification<T> Not();
    }

Implementacja abstrakcyjnej klasy bazowej wykorzystuje wzorzec kompozyt.

.. code-block:: c#

    public abstract class CompositeSpecification<T> : ISpecification<T>
    {
        public abstract bool IsSatisfiedBy(T candidate);

        public ISpecification<T> And(ISpecification<T> other)
        {
            return new AndSpecification<T>(this, other);
        }

        public ISpecification<T> Or(ISpecification<T> other)
        {
            return new OrSpecification<T>(this, other);
        }

        public ISpecification<T> Not()
        {
            return new NotSpecification<T>(this);
        }
    }

Implementacja klas umożliwiająca logiczne składanie ze sobą reguł.

.. code-block:: c#

    public class AndSpecification<T> : CompositeSpecification<T>
    {
        private ISpecification<T> _leftSpecification;
        private ISpecification<T> _rightSpecification;

        public AndSpecification(ISpecification<T> leftSpecification, 
                                ISpecification<T> rightSpecification)
        {
            _leftSpecification = leftSpecification;
            _rightSpecification = rightSpecification;
        }

        public override bool IsSatisfiedBy(T candidate)
        {
            return _leftSpecification.IsSatisfiedBy(candidate) 
                && _rightSpecification.IsSatisfiedBy(candidate);
        }
    }

    public class OrSpecification<T> : CompositeSpecification<T>
    {
        private ISpecification<T> _leftSpecification;
        private ISpecification<T> _rightSpecification;

        public OrSpecification(ISpecification<T> leftSpecification, 
                               ISpecification<T> rightSpecification)
        {
            _leftSpecification = leftSpecification;
            _rightSpecification = rightSpecification;
        }

        public override bool IsSatisfiedBy(T candidate)
        {
            return _leftSpecification.IsSatisfiedBy(candidate) 
                || _rightSpecification.IsSatisfiedBy(candidate);
        }
    }

    public class NotSpecification<T> : CompositeSpecification<T>
    {
        private ISpecification<T> _innerSpecification;

        public NotSpecification(ISpecification<T> innerSpecification)
        {
            _innerSpecification = innerSpecification;
        }

        public override bool IsSatisfiedBy(T candidate)
        {
            return !_innerSpecification.IsSatisfiedBy(candidate);
        }
    }

Użycie reguł specyfikacji w obiekcie encji:

.. code-block:: c#

    public class CustomerAccount
    {
        private ISpecification<CustomerAccount> _hasReachedRentalThreshold;
        private ISpecification<CustomerAccount> _customerAccountIsActive;
        private ISpecification<CustomerAccount> _customerAccountHasLateFees;

        public CustomerAccount()
        {
            _hasReachedRentalThreshold = new HasReachedRentalThresholdSpecification();
            _customerAccountIsActive = new CustomerAccountStillActiveSpecification();
            _customerAccountHasLateFees = new CustomerAccountHasLateFeesSpecification(); 
        }

        public decimal NumberOfRentalsThisMonth { get; set; }

        public bool AccountActive { get; set; }

        public decimal LateFees { get; set; }

        public bool CanRent()
        {            
            ISpecification<CustomerAccount> canRent = 
                _customerAccountIsActive
                    .And(_hasReachedRentalThreshold.Not())
                    .And(_customerAccountHasLateFees.Not());

            return canRent.IsSatisfiedBy(this);             
        }
    }


Konsekwencje
------------

Zastosowanie wzorca *Composite* umożliwia łączenie logicznych reguł, co pozwala na elastyczne budowanie 
predykatów o złożonej, ale czytelnej funkcjonalności.

Podsumowanie
------------
1. Przydatny do walidacji stanu obiektów biznesowych.
2. Pozwala na łatwe dodawanie lub zmienianie reguł walidacji.

