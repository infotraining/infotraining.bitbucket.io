**********
Wzorce GOF 
**********

.. toctree::
    :maxdepth: 1

    gof-patterns/factory_method
    gof-patterns/builder
    gof-patterns/decorator
    gof-patterns/composite
    gof-patterns/template_method
    gof-patterns/strategy
    gof-patterns/state
    gof-patterns/chain
    gof-patterns/facade