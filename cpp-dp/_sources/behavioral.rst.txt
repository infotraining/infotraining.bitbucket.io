Wzorce czynnościowe
===================

Czynnościowe wzorce projektowe:

* Dotyczą algorytmów i przydzielania zobowiązań obiektom.
* Charakteryzują złożone przepływy sterowania między obiektami, które są trudne do prześledzenia w czasie wykonywania programu.
* Są wykorzystywane do organizowania, zarządzania i łączenia zachowań.

.. toctree::
   :maxdepth: 2

   template
   strategy
   state
   chain
   command
   memento
   observer
   visitor
   mediator
   iterator

