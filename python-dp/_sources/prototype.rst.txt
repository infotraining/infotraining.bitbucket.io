Prototype
=========

**Przeznaczenie**

* Określa rodzaj tworzonych obiektów, używając prototypowego egzemplarza
* Tworzy nowe obiekty, kopiując ten prototyp
* Zapewnia klientowi możliwość generowania obiektów, których typ nie jest znany

**Kontekst**

* Rzeczywiste typy obiektów, które chcemy utworzyć nie są znane
* Klasy, których instancje chcemy tworzyć, są ładowane dynamicznie
* Stan obiektów klasy może przyjmować tylko jedną z kilku dozwolonych wartości

**Problem**

* Chcemy tworzyć nowe egzemplarze obiektów bez wiedzy o tym, jaka jest ich konkretna klasa
* Chcemy uniknąć budowania hierarchii klas fabryk, która jest porównywalna z hierarchią klas produktów


Struktura
---------

..  image:: images/uml-schemes/Prototype.*
    :width: 500px
    :align: center

Uczestnicy
----------

**Prototype** – deklaruje interfejs klonowania się

**ConcretePrototype** – implementuje operację klonowania się

**Client** – tworzy nowy obiekt, prosząc prototyp o sklonowanie się

Współpraca
----------

Klient prosi prototyp o sklonowanie się.

Konsekwencje
------------

1. Dodawanie i usuwanie produktów w czasie wykonywania programu. Prototype ułatwia włączanie do systemu nowych produktów konkretnych przez rejestrowanie prototypowych egzemplarzy u klienta. Jest to bardziej elastyczne rozwiązanie niż w przypadku innych wzorców kreacyjnych.
2. Umożliwia specyfikowanie nowych prototypowych obiektów przez urozmaicanie struktury. Złożone struktury definiowane w trakcie działania programu mogą być również klonowane.
3. Zredukowana liczba podklas.

Implementacja
-------------

Przy implementowaniu prototypów należy rozważyć:

1. Używanie menedżera prototypów – tablicy asocjacyjnej, która przekazuje prototyp pasujący do podanego klucza, mającej operacje do rejestrowania prototypu pod podanym kluczem i do wyrejestrowywania go.
2. Implementowanie operacji *Clone* –  płytka/głęboka kopia obiektu.
   
   .. code-block:: python
   
       class Prototype:
            def __init__(self):
                self.objects = dict()

            def register(self, identifier, obj):
                self.objects[identifier] = obj

            def unregister(self, identifier):
                del self.objects[identifier]

            def clone(self, identifier, **attr):
                found = self.objects.get(identifier)

                if not found:
                    raise ValueError('Incorrect object identifier: {}'.format(identifier))
                
                obj = copy.deepcopy(found)
                obj.__dict__.update(attr)
                return obj

3. Inicjowanie klonów – jeśli w klasach rozważanego prototypu są już zdefiniowane operacje ustalania wartości istotnej części stanu obiektów, to klienci mogą użyć tych operacji bezpośrednio po klonowaniu.


Wzorce pokrewne
---------------

1. **Abstract Factory** – rywalizuje z wzorcem *Prototype*. Można jednak używać obu wzorców razem. *Abstract Factory* może przechowywać zbiór prototypów, z których mają być klonowane obiekty-produkty.
2. **Composite**, **Decorator** – wzorce te są często używane wraz ze wzorcem *Prototype*.

Podsumowanie
------------

1. Wzorzec umożliwia tworzenie nowych obiektów poprzez kopiowanie prototypowego egzemplarza.
2. Klasy, których egzemplarze są klonowane, mogą być specyfikowane w trakcie wykonania programu.
3. Pozwala uprościć hierarchię klas fabryk, która jest porównywalna z hierarchią klas produktów.
