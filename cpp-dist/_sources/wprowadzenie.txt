********************************************
Wprowadzenie do programowania wielowątkowego
********************************************


Co to jest współbieżność
========================

Najprościej

* dwa działania lub więcej, wykonywane jednocześnie

Jeden procesor

* szybkie przełączanie zadań - iluzja równoległości

.. image:: pics/1_rdzen.*


Wiele procesorów

* Sprzętowa równoległość

.. image:: pics/2_rdzenie.*
   :name: Sytuacja idealna

.. image:: pics/2_rdzenie_real.*
   :name: Realnie

Proces
======

* Egzemplarz wykonywanego programu
* Kontener wątków w chronionej przestrzeni adresowej
* Za zarządzanie procesami odpowiada jądro systemu operacyjnego

  - system operacyjny zarządza priorytetami procesów

.. image:: pics/procesy_watki.*


W systemach wielozadaniowych procesy mogą być wykonywane współbieżnie

* systemy wieloprocesorowe lub wielordzeniowe - współbieżność rzeczywista
* jednoprocesorowe - emulacja współbieżności

  - z wywłaszczaniem - pre-emptive multitasking
  - bez wywłaszczania


Sposoby komunikacji
===================

Komunikacja

* Sygnały
* Sokety
* Pliki
* Potoki
* Pamięć współdzielona

.. image:: pics/procesy_watki.*


Wątek
=====

**Wątek**
  niezależny ciąg instrukcji wykonywany współbieżnie w ramach jednego procesu

Wszystkie wątki działające w danym procesie współdzielą  przestrzeń adresową oraz zasoby systemowe

* pamięć
* pliki
* gniazda, itd.

.. image:: pics/watki_w_procesie.*


Wątki są udostępniane przez system operacyjny:

* MS Windows – Win32 API
* Linux, BSD - pthread

Proces czy wątek?
=================

* Procesy są niezależne
* Wątek należy do procesu
* Procesy mają oddzielne przestrzenie adresowe
* Wątki dzielą przestrzenie adresowe.
* Procesy komunikują się ze sobą za pośrednictwem IPC
* Przełączania kontekstu wątków w tym samym procesie jest zwykle szybsze niż między procesami.
* Komunikacja wewnątrz wątków jest mniej kosztowna niż komunikacja międzyprocesowa
* Każdy proces można wykonać w innym komputerze
* Szukanie błędów w programach wielowątkowych jest łatwiejsze
* Jeśli stabilność jest istotna, spróbuj użyć procesów
* Jeśli wątki dzielą zasoby, które nie mogą być użyte przez wiele procesów jednocześnie, należy korzystać z wątków
  - albo zapewnić IPC
* Jeśli wykorzystywane są zasoby, które są dostępne tylko na zasadzie „jeden na proces”, należy wybrać proces
* Jedną z największych różnic między wątkami a procesami jest to, że wątki wykorzystują konstrukcje
  oprogramowania do ochrony struktur danych, procesy używają sprzętu


Po co używać współbieżności
===========================

* Rozdzielenie odpowiedzialności
* Wydajność

* Rozdzielenie odpowiedzialności
  - Grupowanie powiązanego kodu
  - Rozdzielanie odrębnych operacji gdy mają się odbywać w tym samym czasie

* Wydajność
  - Wykonywanie wielu operacji jednocześnie
  - *"The free lunch is over"*
  - Podział zadań na części i wykonywanie równolegle
  - Trywialnie równoległe


Kiedy nie używać współbieżności?
================================

* Gdy korzyści nie są warte kosztów
* Kod wielowątkowy jest dużo bardziej skomplikowany
* Większa złożoność, więcej błędów
* Narzuty związane z zarządzniem wątkami
* Zbyt wiele wątków uruchomionych na raz
  - zużycie zasobów systemu operacyjnego
  - system jako całość będzie działał wolniej