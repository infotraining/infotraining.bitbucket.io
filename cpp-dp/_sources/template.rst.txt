Template method
===============

**Przeznaczenie**

* Definiuje szkielet algorytmu jako operację, odkładając definicję niektórych kroków algorytmu do klas pochodnych
* Definiując niektóre z kroków algorytmu za pomocą operacji abstrakcyjnych, metoda szablonowa ustala ich kolejność, ale umożliwia podklasom zmianę tych kroków zależnie od ich potrzeb

**Kontekst**

* Istnieje algorytm wymagający zmiany implementacji poszczególnych kroków

**Problem**

* Chcemy jednorazowo zaimplementować stałą część algorytmu i pozostawić klasom pochodnym zaimplementowanie zachowania, które może się zmieniać
* Chcemy zdefiniować metodę, która w wybranych miejscach wywołuje operacje-punkty zaczepienia, umożliwiając tym samym rozszerzanie klas tylko w tych miejscach

Struktura
---------

..  image:: images/uml-schemes/Template.*
    :width: 400px
    :align: center

Uczestnicy
----------

**AbstractClass**

* definiuje abstrakcyjne **operacje pierwotne**, przedefiniowywane przez podklasy w celu zaimplementowania kroków algorytmu
* implementuje metodę szablonową definiującą szkielet algorytmu
* wzorzec *Template* wywołuje operacje pierwotne, jak również operacje zdefiniowane w *AbstractClass* lub operacje innych obiektów

**ConcreteClass** - implementuje operacje pierwotne w celu wykonania specyficznych dla podklasy kroków algorytmu

Współpraca
----------

*ConcreteClass* (klasa konkretna) polega na *AbstractClass* (klasie abstrakcyjnej) w kwestii implementacji niezmiennych kroków algorytmu.

Konsekwencje
------------

1. Użycie metod szablonowych jest podstawową techniką stosowaną w celu zagwarantowania możliwości ponownego wykorzystania kodu.
2. Metody szablonowe prowadzą do odwróconej struktury sterowania – klasa bazowa wywołuje operacje klasy pochodnej, a nie odwrotnie.
3. Metody szablonowe wywołują:

* operacje konkretne – z *ConcreteClass* lub z klas klienta
* operacje konkretne z *AbstractClass* – te, które są na ogół przydatne dla podklas
* operacje pierwotne – operacje abstrakcyjne
* metody wytwórcze
* operacje punkty-zaczepienia, zapewniające zachowanie domyślne, które może być rozszerzane przez klasy pochodne (domyślna implementacja operacji punkt zaczepienia często nic nie robi)

Implementacja
-------------

1. Stosowanie mechanizmów sterowania dostępem z C++. W języku C++ operacje pierwotne wywoływane przez metodę szablonową można zadeklarować jako składowe chronione, co gwarantuje, że będą wywoływane jedynie przez metodę szablonową. Operacje pierwotne, które muszą być przedefiniowane, deklaruje się jako czysto wirtualne. Sama metoda szablonowa nie powinna być przedefiniowywana – można ją deklarować jako niewirtualną funkcję składową.
2. Minimalizowanie liczby operacji pierwotnych.
3. Konwencje dotyczące nadawania nazw.

Wzorce pokrewne
---------------

1. **Strategy** – zarówno wzorzec *Strategy* jak i *Template* dokonują hermetyzacji algorytmów odpowiednio wykorzystując kompozycję i dziedziczenie.
2. **Factory method** – jest szczególnym przypadkiem wzorca *Template*.

Podsumowanie
------------

1. Definiuje szkielet danego algorytmu w określonej metodzie, przekazując realizację niektórych kroków algorytmu do klas podrzędnych.
2. Pozwala klasom podrzędnym na redefiniowanie pewnych kroków algorytmu, ale jednocześnie uniemożliwia zmianę jego struktury.
3. Klasy abstrakcyjne wzorca *Template* mogą definiować metody rzeczywiste, metody abstrakcyjne oraz metody-punkty zaczepienia.
4. Reguła Hollywood – proces podejmowania decyzji powinien być umieszczony w modułach wysokiego poziomu , które mogą samodzielnie decydować, jak i kiedy wywoływać moduły niskiego poziomu.
