.. TDD w języky C# documentation master file, created by
   sphinx-quickstart on Mon Jul 22 12:50:05 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Programowanie w języku C++
==========================

Contents:

.. toctree::
   :maxdepth: 2
   
   basics
   functions
   code-organization
   pointers-arrays
   dynamic-memory
   oop-in-cpp
   raii
   operators
   polymorphism
   move
   exceptions
   io-streams
   templates
   extra/lambdas
   .. extra/constexpr
   

