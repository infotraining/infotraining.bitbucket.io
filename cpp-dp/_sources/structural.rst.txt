Wzorce strukturalne
===================

**Wzorce strukturalne** wiążą się ze składaniem klas i obiektów w większe struktury.

**Klasowe wzorce strukturalne** wykorzystują dziedziczenie do składania interfejsów lub implementacji.

**Obiektowe wzorce strukturalne** nie wykorzystują składania interfejsów lub implementacji, a opisują sposoby składania obiektów w celu uzyskiwania nowej funkcjonalności.

.. toctree::
    :maxdepth: 2

    adapter
    decorator
    composite
    proxy
    facade
    flyweight
    bridge
