
***************************************
Zaawansowane programowanie w języku C++
***************************************

Contents:

.. toctree::
   :maxdepth: 2
   
   move 
   smart-ptr
   templates
   traits-policies
   type-traits
   tag-dispatch
   sfinae 
   variadic-templates
   tuples   
   index-sequences
   constexpr