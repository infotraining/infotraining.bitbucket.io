**************
Biblioteka Any
**************

Klasa ``boost::any``
====================

Boost.Any umożliwia:

* bezpieczne (typowane) mechanizmy przechowywania i odwoływania się do wartości dowolnych typów - bezpieczny typizowany odpowiednik ``void*``
* przechowywanie elementów heterogenicznych w kontenerach biblioteki standardowej
* przekazywanie wartości dowolnych typów pomiędzy warstwami, bez konieczności wyposażania warstw pośredniczących w jakąkolwiek wiedzę o tych typach

Klasa ``boost::any`` służy do przechowywania i udostępniania wartości dowolnych typów, ale pod warunkiem znajomości tych typów, a więc z zachowaniem zalet i bezpieczeństwa typowania.

Typy przechowywane w ``boost::any`` muszą spełniać następujące warunki:

* muszą umożliwiać kopiowanie
* muszą umożliwiać przypisywanie (publiczny operator przypisania)
* nie mogą rzucać wyjątków z destruktora (to jest wymóg odnośnie wszystkich typów użytkownika w C++)

Interfejs klasy ``boost::any``
------------------------------

.. function:: any()

    domyślny konstruktor, tworzący pusty egzemplarz obiektu klasy any 

.. function:: any(const any& other)
    
    konstruktor kopiujący

.. function:: template <typename ValueType> any(const ValueType& value)

    szablonowa wersja konstruktora do tworzenia obiektu przechowywującego kopię argumentu typu ``ValueType`` 

.. function:: ~any()

    destruktor usuwający obiekt usuwa również zawartą w nim wartość, z tym, że jeśli przechowywany jest surowy wskaźnik, usunięciu nie towarzyszy wywołanie operatora ``delete`` 

.. function:: any swap(any& other)

    wymienia wartości przechowywane pomiędzy dwoma obiektami klasy ``any`` 

.. function:: any& operator=(const any& other)

    jeśli obiekt nie jest pusty, operator przypisania powoduje zarzucenie przechowywanej wartości i przyjęcie kopii wartości przechowywanej w ``other``

.. function:: template <typename ValueType> any operator=(const ValueType& value)
    
    szablonowa wersja operatora przypisania

.. function:: bool empty() const
    
    sygnalizuje stan egzemplarza ``any``, zwracając ``false``, jeśli egzemplarz przechowuje jakąkolwiek wartość

.. function:: const std::type_info& type() const

    opisuje typ przechowywanej wartości 

Funkcje zewnętrzne
------------------

Dwie wersje funkcji szablonowej ``any_cast``:

.. function:: template<typename ValueType> ValueType any_cast(const any& operand)
        
    funkcja ``any_cast`` udostępnia wartość przechowywaną w obiekcie ``any``. 
    Argumentem wywołania jest obiekt ``any``, którego wartość ma zostać wyłuskana. Jeśli parametr szablonu funkcji ``ValueType`` nie odpowiada właściwemu typowi przechowywanego elementu rzucany jest wyjątek ``boost::bad_any_cast``

.. function:: template<typename ValueType> ValueType* any_cast(const any* operand)

    przeciążona wersja ``any_cast``, przyjmująca wskaźniki obiektów i zwracająca wskaźniki wartości przechowywanych w ``any``. Jeśli typ ``ValueType`` nie odpowiada typowi właściwemu typowi wartości przechowywanej, zwracany jest wskaźnik pusty.

Stosowanie ``boost::any``
=========================

.. code:: c++

    boost::any a;

    a = std::string("Tekst...");
    a = 42;
    a = 3.1415;

    double pi = boost::any_cast<double>(a); // ok.

    std::string s = boost::any_cast<std::string>(a); // rzuca wyjatek boost::bad_any_cast 

    MyClass* pmc = boost::any_cast<MyClass>(&a); // zwraca NULL

    if (pmc)
    	pmc->do_stuff();
    else 
    	std::cout << "Niepoprawna konwersja any.\n"; 

Kontenery heterogeniczne z ``boost::any``
=========================================

Klasa ``boost::any`` umożliwia przechowywanie w kontenerach standardowych elementów różnych, niezwiązanych ze sobą typów.

.. code:: c++

    void print_any(boost::any& a)
    {
        // ...
    }

    std::vector<boost::any> store_anything;

    store_anything.push_back(A());
    store_anything.push_back(B());
    store_anything.push_back(C());
    ...
    std::for_each(store_anything.begin(), store_anything.end(), &print_any);

Wskaźniki w obiektach ``any``
=============================

Po wydobyciu przy pomocy ``any_cast`` z obiektu wskaźnika należy sprawdzić, czy wskaźnik da się poprawnie wyłuskać.

.. code:: c++

    boost::any a(static_cast<string*>(0));

    if (!a.empty())
    {
        try
        {
            string* p = boost::any_cast<string*>(a);

            if (p)
                cout << *p << endl;
            else
                cout << "Obiekt any zawiera pusty wskaźnik!" << endl;
        }
        catch(boost::bad_any_cast&)
        {
            cout << "Nieprawidłowa konwersja any.\n";
        }
    }

Obiekty typu ``any`` w algorytmach standardowych
================================================

Algorytmy standardowe mogą być wykonywane na heterogenicznych kontenerach zawierających obiekty typu ``any``.

.. code:: 

    using namespace std;

    // predykat
    bool is_int(const boost::any& a) 
    {
    	return typeid(int) == a.type();
    }

    vector<boost::any> a;
    ...
    vector<boost::any> b;

    remove_copy_if(a.begin(), a.end(), back_inserter(b), &is_int);