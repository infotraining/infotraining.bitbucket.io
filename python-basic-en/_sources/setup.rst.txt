******************
Setup Instructions
******************

Python
======

Go to https://www.anaconda.com/products/individual and download Python version 3.x installer, suitable for your operating system. 

Install Python 3 using all of the defaults for installation. Make sure to check "Make Anaconda the default Python".

Visual Studio Code
==================

Go to https://code.visualstudio.com/Download and download Visual Studio Code suitable for your operating system. 

Install Visual Studio Code.

Launch installed VSC. Press Ctrl+Shift+X to manage extensions. Install the following extensions from the marketplace:

 - Python (from Microsoft)
 - Live Share (from Microsoft)

Follow on-screen instructions, additional actions are sometimes required.

Alternatives
============

Python
------

If you are having trouble with above solution, you can install the Python interpreter in a different way.
The training is prepared with Python version 3 in mind, and that version should be installed.
(Eg Mac OS X uses Python 2.7 by default)

Sufficient environment is provided by the `official Python website <https://www.python.org/>`_.
In the *Downloads* section, select "Latest Python 3 Release" for the appropriate operating system.

If you're using Linux, you can install Python through the package manager.

For Ubuntu, the following command should suffice::

    $ sudo apt-get install python3 python3-pip

If you are using Mac OS X, you can use `Homebrew <https://brew.sh/#install>`_.
Then install::

    $ brew install python

You can also use the instructions contained e.g. `here <https://docs.python-guide.org/starting/installation/>`_.


Visual Studio Code
------------------

Live Share technology is also available for Visual Studio 2019.