<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <title>Asyncio</title>
    <link rel="shortcut icon" href="./favicon.ico"/>
    <link rel="stylesheet" href="./dist/reveal.css" />
    <link rel="stylesheet" href="./dist/theme/white.css" id="theme" />
    <link rel="stylesheet" href="./css/highlight/github-gist.css" />
    <link rel="stylesheet" href="./_assets/headers.css" />

  </head>
  <body>
    <div class="reveal">
      <div class="slides"><section  data-markdown><script type="text/template">
<!--
how to run:
npm install reveal-md
reveal-md asyncio_slides.md -w
https://github.com/webpro/reveal-md
-->

![asyncio](imgs/asyncio.png)

### Asynchronous Python programming

Leszek Tarkowski, 2021
</script></section><section  data-markdown><script type="text/template">
## Program

1. Introduction
2. Concurrency in Python
3. Asyncio
4. Testing asynchronous code
5. Useful libraries
5. Asyncio - brief history

</script></section><section  data-markdown><script type="text/template">
# Introduction

### Concurrency

![concurrent](imgs/concurrent.png)<!-- .element width="60%" -->

### Parallelism

![parallel](imgs/parallel.png)<!-- .element width="60%" -->

<aside class="notes"><p>concurrency - współbieżność; parallelism - równoległość</p>
</aside></script></section><section  data-markdown><script type="text/template">
    Concurrency is about dealing with lots of things at once.

    Parallelism is about doing lots of things at once.

    Not the same, but related.

    One is about structure, one is about execution.

    Concurrency provides a way to structure a solution to solve a problem that may (but not necessarily) be parallelizable.

    ---

    *Rob Pike, co-inventor of the Go language*
</script></section><section  data-markdown><script type="text/template">
## Concurrency options in Python

 - **treads** - `threading` or `concurrent.futures` libraries

 - **sub-processes** - `multiprocessing` or `concurrent.futures.ProcessPoolExecutor`

 - **asynchronous I/O** - latests Python addition, asynchronous I/O operations using `asyncio` library
</script></section><section  data-markdown><script type="text/template">
## Threads pros/cons

Pros:

 - **availability** - ubiquitous

Cons:

 - **safety** - threads can cause some(!) unexpected behavior

 - **GIL:** - Global Interpreter Lock makes them useless for CPU-bound applications

 - **memory consumption** - each thread consumes 8MB RAM

 - **launch time** - OS needs time for thread creation

### GIL

Global Interpreter Lock:

 - one for the interpreter instance
 - when using threads (`threading`) we are limited to one CPU due to locking
 - lock is released:
   - every 5ms
   - by some functions - I/O, `time.sleep()`, `numpy`, etc.

In practice, when using threads **GIL doesn't influence I/O performance, but *CPU-bound* code is essentially single-threaded**.
</script></section><section  data-markdown><script type="text/template">
## Processes pros/cons

Pros:

 - **not affected by GIL** - ideal for CPU-bound tasks

Cons:

 - **communication overhead** - significant in some cases

 - **memory consumption** - similar to threads (8MB typically)

 - **launch time** - even larger than in case of threads
</script></section><section  data-markdown><script type="text/template">
## Asynchronous I/O

 - single-thread, single-process model
 - *cooperative multitasking*
 - not using threads/processes, but can cooperate with them
 - uses "suspendable" functions, that can wait for I/O operations
 - during waiting, other functions can be executed/scheduled to run
</script></section><section  data-markdown><script type="text/template">
## Async vs sync

![parallel](imgs/async-sync.png)<!-- .element width="60%" -->

</script></section><section  data-markdown><script type="text/template">
| 1 CPU cycle                    | 0.3 ns    | 1 s         |
|--------------------------------|-----------|-------------|
| Level 1 cache access           | 0.9 ns    | 3 s         |
| Level 2 cache access           | 2.8 ns    | 9 s         |
| Level 3 cache access           | 12.9 ns   | 43 s        |
| Main memory access             | 120 ns    | 6 min       |
| Thread creation                | 20 μs     | 1 day       |
| Solid-state disk I/O           | 50-150 μs | 2-6 days    |
| Rotational disk I/O            | 1-10 ms   | 1-12 months |
| Internet: SF to NYC            | 40 ms     | 4 years     |
| OS virtualization reboot       | 4 s       | 423 years   |

  *Jeff Atwood - codinghorror.com*
</script></section><section  data-markdown><script type="text/template">
## Coroutines

Python 3.5 introduced special objects/functions, called **coroutines**.

Coroutines allow to write a function that does not block when waiting for a result.
Blocking operation (eg. I/O operations) can be a bottleneck of the application.

They are similar to "normal" functions or generators.

Special object/manager, called "loop" is used to launch and synchronize them.
</script></section><section  data-markdown><script type="text/template">
## Hello async

```python
import asyncio

async def main():
    await asyncio.sleep(0)
    return 42

res = asyncio.run(main())
print(res)
```

### components

`async def` - native coroutine | asynchronous generator

`await` - point when execution is halted, loop takes control, and coroutine waits for the result

```python
async def g():
    r = await f()
    return r
```
</script></section><section  data-markdown><script type="text/template">
## `async` keyword

`async def` defines the coroutine, in the body `await`, `return` or `yield` can be used

 - `await` and/or `return` - coroutine
 - `yield` asynchronous generator

`await` can be used only in coroutines
</script></section><section  data-markdown><script type="text/template">
## `await` keyword

The new keyword await always takes a parameter and will accept only an object that is awaitable, which is defined as one of these:

 - A coroutine (i.e. the result of a called async def function)
 - Any object implementing the `__await__()` special method returning an iterator

```python
async def get_data():
    await asyncio.sleep(1.0)
    return 42

async def process_data():
    result = await get_data()
    return result * 2

asyncio.run(process_data())
## returns 84
```

<aside class="notes"><p>Example - synchronous vs asynchronous</p>
</aside></script></section><section  data-markdown><script type="text/template">
## `asyncio.run`

*since Python 3.7*

It should be used as a **main entry point for asyncio programs**, and should ideally only be called once.

This function runs the passed coroutine, taking care of managing the asyncio event loop, finalizing asynchronous generators, and closing the threadpool.

This function always creates a new event loop and closes it at the end.

```python
async def coro():
   await asyncio.sleep(1)
   return 1

asyncio.run(coro())
```
</script></section><section  data-markdown><script type="text/template">
### Run coroutine - before Python 3.7

```python
loop = asyncio.get_event_loop()
try:
    loop.run_until_complete(main())
finally:
    loop.close()

```
</script></section><section  data-markdown><script type="text/template">
## Event loop

![parallel](imgs/event_loop.png)<!-- .element width="60%" -->

</script></section><section  data-markdown><script type="text/template">
## Coroutines - Examples

```python
async def coro(x):
    y = await another_coro(x)  # OK
    return y

async def async_gen(a):
    yield a  # OK - asynchronous generator

async def bad_gen(x):
    yield from gen(x)  # SyntaxError

def bad_function(x):
    y = await coro(x)  # SyntaxError - `async def` missing
    return y
```

```python
async def coroutine(*args, **kwargs):
    pass

assert asyncio.iscoroutine(coroutine())
assert asyncio.iscoroutinefunction(coroutine)
```
</script></section><section  data-markdown><script type="text/template">
## `asyncio` socket streams

Streams are high-level async/await-ready primitives to work with network connections. Streams allow sending and receiving data without using callbacks or low-level protocols and transports.

asyncio.open_connection - Establish a network connection and return a pair of (reader, writer) objects

```python
reader, writer = await asyncio.open_connection(
        '127.0.0.1', 8888)

data = await reader.read(100)
writer.write(data)
await writer.drain()
writer.close()
```

asyncio.start_server - Start a socket server

```python
server = await asyncio.start_server(
        handle_coroutine, '127.0.0.1', 8888)
await server.serve_forever()
```
</script></section><section  data-markdown><script type="text/template">
## Multiple coroutines

Launch in synchronized way

```python
async def coro(text):
    print(await asyncio.sleep(1, text))

async def main():
    await coro("after 1 second")
    await coro("after 2 seconds")
    await coro("after 3 seconds")

asyncio.run(main())
```
</script></section><section  data-markdown><script type="text/template">
## Multiple coroutines - `gather`

Use `asyncio.gather`

```python
async def coro(text, res):
    print(await asyncio.sleep(1, text))
    return result

async def main():
    workload = [
        coro("after 1 second",1),
        coro("after 1 second",2),
        coro("after 1 second",3),
    ]
    results = await asyncio.gather(*workload)
    print(results)

asyncio.run(main())
```

<aside class="notes"><p>The entrance order is not necessarily the order in which the coroutines/futures are scheduled.</p>
</aside></script></section><section  data-markdown><script type="text/template">
## Multiple coroutines - `wait`

Use `asyncio.wait(iterable, timeout, return_when)` to run awaitable objects in the iterable and block until the condition specified by return_when.

`retun_when` = FIRST_COMPLETED | FIRST_EXCEPTION | ALL_COMPLETED

```python
done, pending = await asyncio.wait(aws, timeout=5)
```
</script></section><section  data-markdown><script type="text/template">
## Multiple coroutines - `as_completed`

Use `asyncio.as_completed(iterable)` runs awaitable objects in the iterable. Return an iterator of coroutines.

Each coroutine returned can be awaited to get the earliest next result from the iterable of the remaining awaitables.

```python
for coro in asyncio.as_completed(aws):
    earliest_result = await coro
    # ...
```
</script></section><section  data-markdown><script type="text/template">
## `awaitable` concept

Using `await` is only possible when on the right side is `awaitable` object.

 - `async def` coroutine
 - `Tasks` - created by `asyncio.create_task()`
 - `Futures` - proxy for future results

Can be check:
```inspect.isawaitable```

</script></section><section  data-markdown><script type="text/template">
## The event loop in asyncio

 - handles all the switching between coroutines
 - catches StopIteration exceptions
 - and more (e.g. listening to sockets & file descriptors for events)

There are two ways to get the event loop:

  - Recommended - since Python 3.7

    `asyncio.get_running_loop()`

    callable from inside the context of a coroutine

    provides the current running event loop

  - `asyncio.get_event_loop()`

    callable from anywhere

    will fail if called inside a new thread

Introspection:
 - `loop.is_running()`
 - `loop.is_closed()`
</script></section><section  data-markdown><script type="text/template">
## Future

`Future` is class represents a state of something that is interacting with a loop

 - provides a safe channel to pass the final result of some activity:

    ```python
    f.set_result(value)  # sets the value
    f.result()           # obtains it
    ```
 - supports the cancellation mechanism
    ```
    f.cancel()
    f.cancelled()  # checks if an action has been cancelled
    f.done()       # checks the completion status
   ```

 - supports continuations by setting additional callbacks that will be run when the future completes
</script></section><section  data-markdown><script type="text/template">
```python
from asyncio import Future

f = Future()
f.done()

## False

async def main(f: asyncio.Future):
    await asyncio.sleep(1)
    f.set_result('I have finished')

f = asyncio.Future()
f.done()

## False

await main(f)
f.done()
```
</script></section><section  data-markdown><script type="text/template">
## Task

 - `Task` is a subclass of `Future`
 - It represents a running coroutine
 - Since Python 3.8 it is not allowed to call set_result() on Task - an attempt will raise RuntimeError

  ---

### Creating tasks

```python
asyncio.create_task(some_coroutine(i))
```

```python
async def coro():
    print(await asyncio.sleep(1, result="finished!"))

async def main():
    task = asyncio.create_task(coro())
    await task

asyncio.run(main())
```
</script></section><section  data-markdown><script type="text/template">
## coroutine launching

 - `await`
 - `asyncio.ensure_future`

low level:

 - `loop.create_task`
 - `asyncio.create_task`
</script></section><section  data-markdown><script type="text/template">
# Legacy method - `loop.run_until_complete`

```python
async def main():
    pass

loop = asyncio.get_event_loop()

try:
    loop.run_until_complete(main())
finally:
    try:
        loop.run_until_complete(loop.shutdown_asyncgens())
    finally:
        loop.close()
```
</script></section><section  data-markdown><script type="text/template">
## `asyncio.ensure_future()`

`asyncio.ensure_future(coro_or_future, *, loop=None)` is a function that allows to wrap the coroutine in a `Task`

If you pass in a coroutine, it will produce a `Task` instance. This is identical to calling `asyncio.create_task()` and returning the new `Task` instance

If you pass in a `Future` instance (or a `Task` instance, because `Task` is a subclass of `Future`), you get that very same thing returned

It is a helper function intended for framework designers

```python
async def f():
    pass

coro = f()
loop = asyncio.get_event_loop()

task = loop.create_task(coro)
assert isinstance(task, asyncio.Task)

new_task = asyncio.ensure_future(coro)
assert isinstance(new_task, asyncio.Task)

guess_what = asyncio.ensure_future(task)
assert guess_what is task
```
</script></section><section  data-markdown><script type="text/template">
## `async.wait_for`

Launch task with limited time for execution

```python
try:
    asyncio.wait_for(coro, delay)
except asyncio.TimeoutError:
    print("time out")
```
</script></section><section  data-markdown><script type="text/template">
## `task.cancel()`

Task running on the loop can be cancelled

```python
async def cancellable(delay=10):
    loop = asyncio.get_running_loop()
    try:
        now = loop.time()
        print(f"Sleeping from {now} for {delay} seconds ...")
        await asyncio.sleep(delay, loop=loop)
        print(f"Slept {delay} seconds ...")
    except asyncio.CancelledError:
        print(f"Cancelled at {now} after {loop.time()-now} seconds")
async def main():
    coro = cancellable()
    task = asyncio.create_task(coro)
    await asyncio.sleep(3)
    task.cancel()
asyncio.run(main())
```
</script></section><section  data-markdown><script type="text/template">
## cancel prevention

use:

```python
prot_task = asyncio.shield(coro)
```

instead of:

```python
unprot_task = asyncio.create_task(coro)
```
</script></section><section  data-markdown><script type="text/template">
# Coroutine testing
</script></section><section  data-markdown><script type="text/template">
## Coroutine testing

The `aiounittest` is a helper library for writing a tests of the asynchronous code

Since the Python 3.8 it is recommended to use `unittest.IsolatedAsyncioTestCase`

There is also support for testing in `pytest` library, named `pytest-asyncio`, providing decorator
</script></section><section  data-markdown><script type="text/template">
# pytest

Decorator provides support for testing coroutines

```python
@pytest.mark.asyncio
async def test_some_asyncio_code():
    res = await library.do_something()
    assert b'expected result' == res
```
</script></section><section  data-markdown><script type="text/template">
# unittest

New base class `unittest.IsolatedAsyncioTestCase`

```python
async def AsyncConnection():
    yield True

import unittest

class TestRequest(unittest.IsolatedAsyncioTestCase):

    async def asyncSetUp(self):
        self.connection = await AsyncConnection()

    async def test_get(self):
        response = await self.connection.get("https://example.com")
        self.assertEqual(response.status_code, 200)

    async def asyncTearDown(self):
        await self.connection.close()
```
</script></section><section  data-markdown><script type="text/template">
# Blocking code - what to do
</script></section><section  data-markdown><script type="text/template">
## Blocking code - what to do

Direct usage of blocking calls (eg. popular `request`) will block event loop.

### `run_in_executor`

returns `awaitable`, that after `await` will return `future` of the result

### `asyncio.to_thread`

since Python 3.9
</script></section><section  data-markdown><script type="text/template">
## Blocking code - example

```python
def synchronous():
    pass

async def make_asynchronous_task(pool):
    loop = asyncio.get_running_loop()
    await loop.run_in_executor(pool, synchronous)

async def main()
    pool = ThreadPoolExecutor
    await make_asynchronous_task(pool)
```

```python
async def make_asynchronous_task(pool):
    asyncio.to_thread(synchronous)
```
</script></section><section  data-markdown><script type="text/template">
# Async iterators and generators
</script></section><section  data-markdown><script type="text/template">
## Async iterators - `async for`

Async iterators require implementing several magic methods

 - `def __aiter__()` must return an object that implements `async def __next__()`

 - `__anext__()` must return a value for each iteration and raise `StopAsyncIteration` when finished


```python
class OneAtATime:
    def __init__(self, redis, keys):
        self.redis = redis
        self.keys = keys

    def __aiter__(self):
        self.ikeys = iter(self.keys)
        return self

    async def __anext__(self):
        try:
            k = next(self.ikeys)
        except StopIteration:
            raise StopAsyncIteration

        value = await self.redis.get(k)
        return (k, value)
```
</script></section><section  data-markdown><script type="text/template">
## Async generators

 - Async generators are `async def` functions that have `yield` keywords inside

```python
async def one_at_a_time(redis, keys):
    for key in keys:
        value = await redis.get(key)
        yield key, value

async def worker(id):
    redis = await create_redis(('localhost', 6379))
    async for key, value in one_at_a_time(redis, keys):
        print(f'#{id}: {key} - {value}')
```
</script></section><section  data-markdown><script type="text/template">
## Async comprehensions

```python
async def worker():
    redis = await create_redis(('localhost', 6379))
    result = [kv async for kv in one_at_a_time(redis, keys)]
    print(result)

    result = {k: v async for k, v in one_at_a_time(redis, keys)}
    print(result)

```
</script></section><section  data-markdown><script type="text/template">
# Asynchronous Context Managers
</script></section><section  data-markdown><script type="text/template">
## `async with` - asynchronous context managers

```python
# Perform some IO operations synchronously
with RemoteResource(*some_parameters) as connection:
    connection.send(some_data)
    new_data = connection.recv()

# Perform the same IO operations asynchronously
async with RemoteResource(*some_parameters) as connection:
    await connection.send(some_data)
    new_data = await connection.recv()
```
</script></section><section  data-markdown><script type="text/template">
## `async with` - asynchronous context managers

Context managers in many scenarios require system resources (e.g. network connections) to be opened and closed within a well defined scope.

Asynchronous context managers require async with clause

Their operation is driven by the coroutines - `__aenter__()` and `__aexit__()`


```python
async with AsyncCM as c:
    c.do_something()
```
Is the same as:

```python
c = await AsyncCM.__aenter__()
try:
    c.do_something()
except Exception as e:
    if not await AsyncCM.__aexit__(type(e), e, e.__traceback__):
        raise e
else:
    await AsyncCM.__aexit__(None, None, None)
```
</script></section><section  data-markdown><script type="text/template">
## `async with` - using contextmanager

`contextlib` provides decorator

```python
@asynccontextmanager
async def AsyncCM(param):
    # start == __aenter__
    yield c
    # finish __aexit__
```

</script></section><section  data-markdown><script type="text/template">
# Selected `asyncio` elements
</script></section><section  data-markdown><script type="text/template">## `asyncio.Queue`

asyncio queues are designed to be similar to classes of the queue module.

```python
async def worker(name, queue):
    while True:
        sleep_for = await queue.get()   # If queue is empty, wait
        await asyncio.sleep(sleep_for)

async def main():
    queue = asyncio.Queue()
    for sleep_for in range(20):
        await queue.put(sleep_for)      # If queue is full, wait
```
</script></section><section  data-markdown><script type="text/template">
## `callback` functions

 - `call_soon`
 - `call_soon_threadsafe`
 - `call_at`
 - `call_later`
</script></section><section  data-markdown><script type="text/template">
# Asyncio in threads/processes

To use `asyncio` inside spawned process or thread we need a new event loop.

```python
def worker(...)
    loop = asyncio.new_event_loop()
    try:
        loop.run_until_complete(asyncio.gather(*tasks, loop=loop))
    except:
        loop.stop()
    finally:
        loop.close()

```


<aside class="notes"><p>in event_loop_in_threads.py</p>
</aside></script></section><section  data-markdown><script type="text/template">
## Synchronization primitives

Asyncio synchronization primitives are designed to be similar to those of the threading module

Asyncio primitives are not thread-safe

Following classes are available:

 - `Lock`
 - `Event`
 - `Condition`
 - `Semaphore`/`BoundedSemaphore`
</script></section><section  data-markdown><script type="text/template">
## `asyncio.Lock`

```python

lock = asyncio.Lock()

async with lock:
    # context manager
    shared_state.work()
```
</script></section><section  data-markdown><script type="text/template">
## `asyncio.Event`

```python
event = asyncio.Event()

async def coro1():
    #...
    event.wait()
    #...

async def coro2():
    #...
    event.set()
    #...
```
</script></section><section  data-markdown><script type="text/template">
## `asyncio.Condition`

Condition object combines the functionality of an `Event` and a `Lock`.

```python
cond = asyncio.Condition()

# ... later
async with cond:
    await cond.wait()

# ... to wake up
cond.notify()  # or
cond.notify_all()
```
</script></section><section  data-markdown><script type="text/template">
## `asyncio.Semaphore`

A semaphore manages an internal counter which is decremented by each `acquire()` call and incremented by each `release()` call.

When `acquire()` finds that counter is zero, it blocks, waiting until some task calls `release()`.

</script></section><section  data-markdown><script type="text/template">
## Transports and Protocols

Transports and Protocols are used by the low-level event loop APIs such as `loop.create_connection()`.
They use callback-based programming style and enable high-performance implementations of network or IPC protocols (e.g. HTTP).

Essentially, transports and protocols **should only be used in libraries and frameworks** and never in high-level asyncio applications.

Barebone functionality can be achieved by implementing:

`BaseProtocol.connection_made(transport)` -  Called when a connection is made.

`BaseProtocol.connection_lost(exc)` - Called when the connection is lost or closed.

`Protocol.data_received(data)` - Called when some data is received. data is a non-empty bytes object containing the incoming data.
</script></section><section  data-markdown><script type="text/template">
## Protocol example

```python
class EchoClientProtocol(asyncio.Protocol):
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost

    def connection_made(self, transport):
        transport.write(self.message.encode())
        print('Data sent: {!r}'.format(self.message))

    def data_received(self, data):
        print('Data received: {!r}'.format(data.decode()))

    def connection_lost(self, exc):
        print('The server closed the connection')
        self.on_con_lost.set_result(True)
```
</script></section><section  data-markdown><script type="text/template">
# Libraries

 - `aiofiles`
 - `aiohttp`
</script></section><section  data-markdown><script type="text/template">
## `aiofiles` library

Library for handling local disk files in asyncio applications.

```python
async with aiofiles.open('filename', mode='r') as f:
    contents = await f.read()
print(contents)
```

Asynchronous iteration is also supported.

```python
async with aiofiles.open('filename') as f:
    async for line in f:
        ...

```
</script></section><section  data-markdown><script type="text/template">
## `aiohttp` library

Asynchronous HTTP Client/Server for asyncio and Python.

```python
import aiohttp
import asyncio

async def main():
    async with aiohttp.ClientSession() as session:
        async with session.get('http://python.org') as response:
            print("Status:", response.status)
            print("Content-type:", response.headers['content-type'])
            html = await response.text()
            print("Body:", html[:15], "...")
```
</script></section><section  data-markdown><script type="text/template">
## Old/new syntax

Since version 3.5, Python has three kinds of coroutines:

- native coroutines

 A coroutine defined with async def. You can delegate from a native coroutine to another native coroutine or to a generator-based coroutine using the await keyword, similar to how classic coroutines use yield from.

 - generator-based coroutines

 A generator function decorated with `@coroutine`, which makes it compatible with the new await keyword, introduced in Python 3.5

 - classic coroutines

 A generator function that consumes data sent to it via my_coro.send(data) calls, and reads that data by using yield in an expression. Classic coroutines can delegate to other classic coroutines using yield from

 **no longer compatibile with asyncio**


Almost equivalent:

```python
@asyncio.coroutine
def up_to_py34_coro():
    """Generator-based, legacy syntax"""
    yield from stuff()

async def since_py35_coro():
    """Native coroutine, new syntax"""
    await stuff()
```

<aside class="notes"><p>starting from 3.10 legacy syntax will be illegal</p>
<hr>
</aside></script></section></div>
    </div>

    <script src="./dist/reveal.js"></script>

    <script src="./plugin/markdown/markdown.js"></script>
    <script src="./plugin/highlight/highlight.js"></script>
    <script src="./plugin/zoom/zoom.js"></script>
    <script src="./plugin/notes/notes.js"></script>
    <script src="./plugin/math/math.js"></script>
    <script>
      function extend() {
        var target = {};
        for (var i = 0; i < arguments.length; i++) {
          var source = arguments[i];
          for (var key in source) {
            if (source.hasOwnProperty(key)) {
              target[key] = source[key];
            }
          }
        }
        return target;
      }

      // default options to init reveal.js
      var defaultOptions = {
        controls: true,
        progress: true,
        history: true,
        center: true,
        transition: 'default', // none/fade/slide/convex/concave/zoom
        plugins: [
          RevealMarkdown,
          RevealHighlight,
          RevealZoom,
          RevealNotes,
          RevealMath
        ]
      };

      // options from URL query string
      var queryOptions = Reveal().getQueryHash() || {};

      var options = extend(defaultOptions, {"controls":true,"progress":true,"hash":false,"respondToHashChanges":true,"mouseWheel":true,"display":"block","viewDistance":10,"width":"90%","height":"95%","BackgroundImage":"imgs/background.png","BackgroundSize":"1920px 1080px"}, queryOptions);
    </script>


    <script>
      Reveal.initialize(options);
    </script>
  </body>
</html>
