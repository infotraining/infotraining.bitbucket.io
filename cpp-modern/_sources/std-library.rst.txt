**********************
Biblioteka standardowa
**********************

.. include::  array.rst

.. include::  hashed-containers.rst

.. include::  tuples.rst

.. include::  any.rst

.. include::  string-view.rst

.. include::  optional.rst

.. include::  variant.rst