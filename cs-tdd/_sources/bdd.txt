***********************
Behavior-Driven Design
***********************

**Behavior-Driven Development** to proces rozwoju oprogramowania bazujący na TDD w połączeniu z ideami zaczerpniętymi
z **Domain-Driven Design** oraz analizy OOP.

    "BDD is a second-generation, **outside-in**, pull-based, **multiple-stakeholder**, multiple-scale,
    **high-automation**, agile methodology. It describes a cycle of interactions with well-defined outputs, resulting
    in the delivery of working, tested **software that matters**."

    -- Dan North

Metodologia BDD została opisana przez Dan'a North'a jako odpowiedź na szereg problemów na które napotykał w trakcie
uczenia TDD.

* Gdzie zacząć process TDD?
* Co testować a co nie testować?
* Ile elementów testować w jednym teście?
* Jak nazywać testy?
* Jak należy rozumieć test, który nie jest zaliczony?

BDD jest metodologią rozwoju oprogramowania, która kładzie nacisk na komunikację przy pomocy zwykłego języka.
Dzięki temu w proces definiowania wymagań wraz z testami akceptacyjnymi mogą być zaangażowani klienci biznesowi.
Język użyty w procesie BDD jest językiem używanym przez klientów ("Ubiquitous Language" z Domain-Driven Design).

BDD czerpie z metodologii Agile definiowanie wymagań przy pomocy "User Stories".
Wymagania zapisane przy pomocy "User Stories" są przekształcane do cech ("features") oraz scenariuszy i stają się
wykonywalnym zestawem testów akceptacyjnych.

Zasady BDD
==========

BDD należy postrzegać jako rozwinięcie technik TDD i ATDD. BDD zakłada, że testy dowolnego elementu oprogramowania
powinny być definiowane w kategoriach oczekiwanego zachowania tej jednostki. W tym przypadku "oczekiwane zachowanie"
zdefiniowane jest przez zbiór wymagań biznesowych - jest to zachowanie, które posiada wartość biznesową dla odbiorcy oprogramowania.
Z tego powodu BDD jest techniką **"Outside-In"**.

.. image:: img/bdd.*
    :align: center

Zasady BDD:

1. Nazwy metod testowych powinny być pełnymi zdaniami.

  * Nazwa metody testowej powinna być pełnym zdaniem w języku domeny biznesowej. Uruchomione testy działają jak
    wykonywalna dokumentacja tworzonego kodu.

2. Należy stosować prosty szablon nazewnictwa.

Style BDD
=========

Możemy wyróżnić dwa główne style, które są praktykowane w metodologii BDD:

1. Styl historii użytkownika (*Plain-Text Stories*)
2. Styl wykonywalnej specyfikacji (*Executable Specification*)

Styl historii użytkownika
-------------------------

Cechy oprogramowania (*features*) są zapisywane jako seria scenariuszy przy pomocy zwykłego języka o określonej gramatyce
(**Gherkin**). Ten styl jest zwykle określany jako **Given/When/Then (GWT)**.

Przykład cechy zapisanej przy pomocy Gherkin'a:

.. code::

    Feature: Addition
       In order to calculate totals
       As a user
       I want to calculate the sum of multiple numbers


    Scenario: Add two numbers
       Given I have entered 10 into the calculator
       And I have entered 50 into the calculator
       When I press add
       Then the result should be 60 on the screen

Tak zapisany scenariusz, może być zamieniony przy pomocy wybranego frameworka BDD na szereg kroków, definiujących test
weryfikujący implementację scenariusza.
Przykład kroku ``Given`` implementowanego przez framework SpecFlow:

.. code:: csharp

    [Given("I have entered (.*) into the calculator")]
    public void GivenIHaveEnteredSomethingIntoTheCalculator(int number)
    {
        _calculator = new Calculator();
        _calculator.Enter(number);
    }

Styl wykonywalnej specyfikacji
------------------------------

W przeciwieństwie do stylu historii użytkownika styl wykonywalnej specyfikacji wykorzystuje bezpośrednio kod źródłowy
do wyrażania oczekiwań w stosunku do tworzonego kodu. Wykonywalna specyfikacja może być implementowana przy pomocy
klasycznych frameworków testów jednostkowych (NUnit, MsTest, xUnit) lub przy pomocy specjalizowanych frameworków BDD takich jak:

* Machine.Specifications
* nspec
* SubSpec (bazuje na xUnit)

Context/Specification
*********************

**Context/Specification** to jeden ze stylów wykonywalnej specyfikacji. Definiuje wymagania dla cech oprogramowania
jako zestaw specyfikacji w danym kontekście użycia. Zachowanie software'u może zostać wyrażone przez kontekst wraz z zestawem
oczekiwanych wyników (testów).

Styl Context/Specification można zaimplementować przy pomocy NUnit i wzorca Template Method:

.. code:: csharp

    [TestFixture]
    public abstract class Context
    {
        [TestFixtureSetUp]
        public void setup()
        {
            establish_context();
            because_of();
        }

        [TestFixtureTearDown]
        public void teardown()
        {
            cleanup();
        }

        protected virtual void establish_context()
        {
        }

        protected virtual void because_of()
        {
        }

        protected virtual void cleanup()
        {
        }
    }

Dodatkowo można utworzyć klasę atrybutu dziedziczącą pa ``TestAttribute``:

.. code:: csharp

    public class ObservationAttribute : TestAttribute {}

Tak utworzona klasa bazowa ``Context`` oraz atrybut ``ObservationAttribute`` mogą zostać wykorzystane do napisania
testów w stylu Context/Specification:

.. code:: csharp

    public class when_adding_two_numbers : Context
    {
        Calculator _calculator;

        protected override void establish_context()
        {
            _calculator = new Calculator();
            _calculator.Enter(2);
            _calculator.PressPlus();
            _calculator.Enter(2);
        }

        protected override void because_of()
        {
            _calculator.PressEnter();
        }

        [Observation]
        public void it_should_return_the_correct_sum()
        {
            Assert.AreEqual(_calculator.Result, 4);
        }
    }

BDD Case Study z użyciem framework'ów SpecFlow i Machine.Specifications
=======================================================================


Utworzenie i przygotowanie projektów
------------------------------------

Należy utworzyć projekty:

* ``BDD.Calculator.Domain``: biblioteka klas
* ``BDD.Calculator.UI.Web``: aplikacja ASP.NET MVC 4

  * referencja do projektu ``BDD.Calculator.Domain``

* ``BDD.Calculator.AcceptanceTests``: biblioteka klas

  * referencja do projektu ``BDD.Calculator.UI.Web``

* ``BDD.Calculator.Domain.Specs``: biblioteka klas

  * referencja do projektu ``BDD.Calculator.Domain``

* ``BDD.Calculator.Web.UI.Specs``: biblioteka klas

  * referencje do projektów ``BDD.Calculator.UI.Web`` oraz ``BDD.Calculator.Domain``

Definiowanie testów akceptacyjnych
----------------------------------

Wybierz projekt ``BDD.Calculator.AcceptanceTests``. W konsoli menadżera pakietów wpisz polecenie instalacji
biblioteki SpecFlow:

.. code::

    PM> Install-Package SpecFlow


Zainstaluj również pakiet NUnit:

.. code::

    PM> Install-Package NUnit

Definiowanie cechy
------------------

Utwórz w projekcie katalog ``Features`` i kliknij na nim prawym przyciskiem myszy, i wybierz opcję ``Add...\New Item``.
W oknie dialogowym wybierz ``SpecFlow Feature File``.

.. image:: img/SpecFlow-feature.*
    :align: center

Podaj nazwę ``AddNumbers.feature``. VS utworzy plik z cechą i przykładowym scenariuszem.

Zmodyfikuj zawartość pliku na:

.. code::

    Feature: AddNumbers
        In order to avoid silly mistakes
        As a math idiot
        I want to be told the sum of two numbers

    Scenario: Navigation to Math Page
        When a user navigates to Math page
        Then a Math page should be displayed

    Scenario: Add two numbers
        Given I have entered 50 as a first number into on the Math Page
        And I have entered 70 as a second number into on the Math Page
        When I press add
        Then the result should be 120 on the screen

Scenariusz 1: Navigation to Math Page
-------------------------------------

Jeśli uruchomimy teraz testy, nie zostaną one zaliczone a w oknie pojawi się następujący komunikat:

.. image:: img/SpecFlow-failed.*
    :align: center

Utwórz folder ``Steps`` i dodaj klasę ``AddNumbersSteps``. Do pliku przekopiuj klasę z komunikatu Test Runner'a.

Następnie dodaj kod do odpowiednich metod testowych.

.. code::

    using System.Web.Mvc;
    using BDD.Calculator.UI.Web.Controllers;
    using NUnit.Framework;
    using TechTalk.SpecFlow;

    namespace BDD.Calculator.AcceptanceTests.Steps
    {
        [Binding]
        public class StepDefinitions
        {
            private ActionResult _result;
            private MathController _mathController;

            [BeforeScenario]
            public void Init()
            {
                _mathController = new MathController();
            }

            [When(@"a user navigates to Math page")]
            public void WhenAUserNavigatesToMathPage()
            {
                _result = _mathController.AddNumbers();
            }

            [Then(@"a Math page should be displayed")]
            public void ThenAMathPageShouldBeDisplayed()
            {
                Assert.IsInstanceOf<ViewResult>(_result);
                Assert.AreEqual("Math Page", _mathController.ViewBag.Title);
            }
        }
    }

Uruchamiamy testy jednostkowe w Visual Studio. Test ``NavigationToMathPage`` nie jest zaliczony.

.. image:: img/SpecFlow-first-test-failed.*
        :align: center

Piszemy kod, który spowoduje, że test zapali się na zielono:

W projekcie ``BDD.Calculator.UI.Web`` w folderze ``Controllers`` tworzymy kontroler ``MathController`` a następnie z
metody akcji ``Math()`` zwracamy widok z tytułem "Math Page". Widok ``Math.cshtml`` należy utworzyć w katalogu ``Views\Math``.

.. code:: csharp

    public class MathController : Controller
    {
        public ActionResult AddNumbers()
        {
            ViewBag.Title = "Math Page";

            return View();
        }
    }

Uruchamiamy testy i tym razem test zapala się na zielono, więc możemy przejść do testów następnego scenariusza.

.. image:: img/SpecFlow-first-test-passed.*
        :align: center

Scenariusz 2: Add two Numbers
-----------------------------

Aby zaimplementować test akceptacyjny dla scenariusza "Add two Numbers" należy dodać definicje kroków scenariusza w
pliku ``AddNumbersSteps.cs``.

.. code:: csharp

    [Binding]
    public class StepDefinitions
    {
        // pozostałe pola

        private AddNumbersModel _model;

        [BeforeScenario]
        public void Init()
        {
            _mathController = new MathController();
            _model = new AddNumbersModel(); // dodano
        }

        [Given(@"I have entered (.*) as a first number into on the Math Page")]
        public void GivenIHaveEnteredAsAFirstNumberIntoOnTheMathPage(int firstValue)
        {
            _model.FirstValue = firstValue;
        }

        [Given(@"I have entered (.*) as a second number into on the Math Page")]
        public void GivenIHaveEnteredAsASecondNumberIntoOnTheMathPage(int secondValue)
        {
            _model.SecondValue = secondValue;
        }

        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            _result = _mathController.AddNumbers(_model);
        }

        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(int result)
        {
            var resultView = _result as ViewResult;
            var model = (AddNumbersModel) resultView.Model;

            Assert.AreEqual(result, model.AdditionResult);
        }
    }

Na tym etapie projekt się nie kompiluje, ponieważ brak implementacji klas ``AddNumbersModel`` oraz przeciążonej metody
``AddNumbers()`` w kontrolerze ``MathController``.

Tworzymy klasę ``AddNumbersModel``:

.. code:: csharp

    namespace BDD.Calculator.UI.Web.Models
    {
        public class AddNumbersModel
        {
            [DisplayName("Number 1")]
            public int FirstValue { get; set; }

            [DisplayName("Number 2")]
            public int SecondValue { get; set; }

            public int AdditionResult { get; set; }
        }
    }

Implementujemy również metodę akcji ``Add`` w kontrolerze ``MathController``:

.. code:: csharp

    [HttpPost]
    public ActionResult AddNumbers(AddNumbersModel model)
    {
        throw new NotImplementedException();
    }

Po uruchomieniu testów otrzymujemy następujący komunikat:

.. image:: img/SpecFlow-second-test-failed.*
        :align: center

Specyfikacja dla kontrolera ``Math``
------------------------------------

Kolejnym krokiem w rozwijaniu aplikacji w metodologii BDD jest zejście z poziomu niezaliczonego testu akceptacyjnego na poziom
testów jednostkowych, które piszemy w stylu "Context/Specification". W tym celu użyjemy frameworka BDD ``Machine.Specification``.

W projekcie ``BDD.Calculator.Web.UI.Specs`` instalujemy pakiety ``Machine.Specifications.MVC`` oraz ``Moq``.

.. code::

    Install-Package Machine.Specifications.MVC4

    Install-Package Moq

Tworzymy klasy specyfikacji wykorzystując framework MSpec:

.. code::

    namespace BDD.Calculator.Web.UI.Specs
    {
        public abstract class WithMathController
        {
            protected static MathController _mathController;
            protected static Mock<ICalculator> _calculator;

            protected WithMathController()
            {
                _calculator = new Mock<ICalculator>();
                _mathController = new MathController(_calculator.Object);
            }
        }

        [Subject(typeof (MathController))]
        public class WhenAddingTwoNumbers : WithMathController
        {
            private static int _firstNumber = 10;
            private static int _secondNumber = 20;
            private static int _expectedNumber = 30;
            private static ActionResult _result;

            private Establish context = () =>
            {
                _firstNumber = 10;
                _secondNumber = 20;

                _calculator.Setup(c =>
                                  c.Add(_firstNumber, _secondNumber)).Returns(_expectedNumber);
            };

            private Because of = () =>
            {
                var model = new AddNumbersModel()
                                { FirstValue = _firstNumber, SecondValue = _secondNumber };

                _result = _mathController.AddNumbers(model);
            };

            private It should_call_add_on_calculator = () =>
            {
                _calculator.Verify(c => c.Add(_firstNumber, _secondNumber));
            };

            private It should_return_view_with_a_model = () =>
            {
                _result.ShouldBeAView().And().ShouldHaveModelOfType<AddNumbersModel>();
            };

            private It should_return_correct_result =
                () =>
                    _result.Model<AddNumbersModel>().AdditionResult
                        .ShouldEqual(_expectedNumber);
        }
    }

Uruchamiamy testy. Wszystkie testy specyfikacji zapalają się na czerwono.

Implementujemy na podstawie specyfikacji metodę akcji ``AddNumbers()``:

.. code::

    [HttpPost]
    public ActionResult AddNumbers(AddNumbersModel model)
    {
        model.AdditionResult = _calculator.Add(model.FirstValue, model.SecondValue);

        return View(model);
    }

Teraz wszystkie testy specyfikacji kontrolera ``Math`` zapalają się na zielono. Możemy przejść do implementacji specyfikacji
dla klasy ``Calculator`` w domenie aplikacji.

Specyfikacja dla kalkulatora ``Calculator``
-------------------------------------------

Instalujemy w projekcie ``BDD.Calculator.Domain.Specs`` pakiet ``Machine.Specifications``.

Definiujemy specyfikację dla klasy ``MathCalculator``:

.. code::

    public class WithMathCalculator
    {
        protected static MathCalculator _mathCalculator;

        public WithMathCalculator()
        {
            _mathCalculator = new MathCalculator();
        }
    }

    [Subject(typeof(MathCalculator), "MathCalculator")]
    public class WhenAddingTwoNumbers : WithMathCalculator
    {
        private static int _firstValue;
        private static int _secondValue;
        private static int _expectedValue;
        private static int _result;


        private Establish context = () =>
        {
            _firstValue = 10;
            _secondValue = 20;
            _expectedValue = 30;
        };

        private Because of = () => _result = _mathCalculator.Add(_firstValue, _secondValue);

        private It should_return_correct_value = () => _result.ShouldEqual(_expectedValue);
    }

Na podstawie specyfikacji implementujemy klasę ``MathCalculator`` w projekcie ``BDD.Calculator.Domain``, tak aby testy
specyfikacji zostały zaliczone:

.. code:: csharp

    public class MathCalculator : ICalculator
    {
        public int Add(int firstNumber, int secondNumber)
        {
            return firstNumber + secondNumber;
        }
    }

Teraz wszystkie specyfikacje są zaliczone. Jedyny test, jaki się nie zapala na czerwono to test akceptacyjny ``AddTwoNumbers``.

Aby test został zaliczony musimy ustalić zależność kontrolera ``MathController`` od klasy ``MathCalculator``.
Najprostszym sposobem jest zdefiniowanie domyślnego konstruktora w następujący sposób:

.. code:: csharp

    public class MathController : Controller
    {
        private readonly ICalculator _calculator;

        public MathController()
        {
            _calculator = new MathCalculator();
        }

        public MathController(ICalculator calculator)
        {
            _calculator = calculator;
        }

        // reszta implementacji kontrolera
     }

Uruchamiamy wszystkie testy. Wszystkie testy, w tym testy akceptacyjne, zostały zaliczone.