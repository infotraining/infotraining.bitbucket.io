*****************************
Nowe typy standardowe w C++17
*****************************

.. toctree::
    :maxdepth: 2

    string-view
    any
    variant
    optional