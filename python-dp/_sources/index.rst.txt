.. cpp-dp documentation master file, created by
   sphinx-quickstart on Tue Dec 09 12:33:11 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=================================
Wzorce projektowe w języku Python
=================================

Spis treści:

.. toctree::
   :maxdepth: 2

   oop-basics
   solid-oop
   design-patterns-intro
   creational
   structural
   behavioral
