std::array<T, N>
================

``std::array<T, N>`` implementuje tablicę o stałym rozmiarze (*fixed size array*).
Spełnia (prawie) wszystkie wymagania dla kontenera przez co jest dużo wygodniejsza w użyciu niż tablica typu *C-style*.

Plik nagłówkowy ``<array>``

.. code-block:: cpp

    namespace std
    {
        template <typename T, size_t Size>
        struct array 
        {
            T _Elems[Size];
        
            using value_type = T;
            using size_type = size_t;
            using difference_type = ptrdiff_t;
            using pointer = T *;
            using const_pointer = const T *;
            using reference = T&;
            using const_reference = const T&;

            using iterator = _Array_iterator<_Ty, _Size>;
            using const_iterator = _Array_const_iterator<_Ty, _Size>;
            
            using reverse_iterator = _STD reverse_iterator<iterator>;   
            using const_reverse_iterator = _STD reverse_iterator<const_iterator>;

            constexpr reference operator[](size_type pos) noexcept 
            {
                return _Elems[pos];
            }

            constexpr iterator begin() noexcept
            {	// return iterator for beginning of mutable sequence
                return (iterator(_Elems, 0));
            }
      

            constexpr iterator end() noexcept
            {	// return iterator for end of mutable sequence
                return (iterator(_Elems, _Size));
            }

            //... rest of implementation
        };
    }

Inicjalizacja
-------------

Ponieważ ``std::array`` jest agregatem, możliwa jest inicjalizacja agregatowa.

Jest też jedynym kontenerem standardowym, którego elementy są inicjalizowane w sposób domyślny (*default initialized*).

.. code-block:: c++

    std::array<int, 4> a; // items of a have undefined value

.. code-block:: cpp

    std::array<int, 4> arr1 = { {} }; // { 0, 0, 0, 0 }
    std::array<int, 4> arr2 = { { 1, 2 } }; // { 1, 2, 0, 0 }
    std::array<int, 4> arr3 = { { 1, 2, 3, 4 } }; // { 1, 2, 3, 4 };
    std::array<int, 6> arr4 = { 1, 2, 3, 4, 5, 6 }; // possible warning

Od C++17 można w celu inicjalizacji wykorzystać mechanizm dedukcji parametrów klasy szablonowej:

.. code-block:: c++

    std::array arr = { 1, 2, 3, 4, 5, 6 }; // std::array<int, 6>

    static_assert(arr.size() == 6);
    static_assert(is_same_v<decltype(arr)::value_type, int>);

    for(const auto& item : arr)
        std::cout << item << ' ';
    std::cout << '\n';

Zwracanie tablicy z funkcji
---------------------------

Jedną z zalet tablic ``std::array`` jest to, że można je zwracać z funkcji:

.. code-block:: c++

    auto cross_product(const std::array<int, 3>& a, const std::array<int, 3>& b) -> std::array<int, 3>
    {
        return {{
            a[1] * b[2] - a[2] * b[1],
            a[2] * b[0] - a[0] * b[2],
            a[0] * b[1] - a[1] * b[0],
        }};
    }

Interoperacyjność z tablicami C
-------------------------------

Metoda ``data()`` zwraca wskaźnik do typu danych przechowywanych w tablicy. Umożliwia to 
współpracę z kodem używającym klasycznych tablic C.

.. code-block:: c++

    namespace LegacyCode
    {
        void use_tab(int* tab, size_t size)
        {
            std::cout << "using tab: ";
            
            for (int* it = tab; it != tab + size; ++it)
                std::cout << *it << " ";
            
            std::cout << '\n';
        }
    }

    std::array arr1 = { 1, 2, 3, 4, 5 };
    LegacyCode::use_tab(arr.data(), arr.size());

Można wykorzystać ``std::array<char>`` jako bufor dla C-string'ów:

.. code-block:: c++

    std::array<char,255> cstr;           // create static array of 41 chars

    strcpy(cstr.data(),"hello, world"); // copy a C-string into the array
    printf("%s\n", cstr.data());        // print contents of the array as C-string


Zamiana danych - swap()
-----------------------

Metoda ``swap()`` umożliwia wymianę danych w tablicach tego samego typu. Złożoność tej operacji jest liniowa.

.. code-block:: cpp

    std::array<int, 4> arr1 = { { 1, 2, -1, 4} };
    std::array<int, 4> arr2 = { {} };

    utils::print(arr1, "arr1: ");
    utils::print(arr2, "arr2: ");

    arr1.swap(arr2);

    cout << "\nAfter swap:\n";
    utils::print(arr1, "arr1: ");
    utils::print(arr2, "arr2: ");

.. code-block:: bash

    arr1: [ 1 2 -1 4 ]
    arr2: [ 0 0 0 0 ]
    Swap:
    arr1: [ 0 0 0 0 ]
    arr2: [ 1 2 -1 4 ]

Interfejs krotki
----------------

Dla typu ``std::array`` zaimplementowany jest interfejs krotki (*tuple interface*). W rezultacie 
można traktować obiekty tablic jako homogeniczne krotki.

.. code-block:: c++

    using ArrayAsTuple = std::array<int, 4>;

    ArrayAsTuple t = { { 1, 2, 3, 4 } };
    static_assert(std::tuple_size<ArrayAsTuple>::value == 4);
    static_assert(std::is_same_v<std::tuple_element<1, ArrayAsTuple>::type, int>);
    assert(std::get<1>(t) == 2);
